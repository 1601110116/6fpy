from boutpy.boutdata import Case
from boutpy.boutdata import collect

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
tl = 1400  # in Alfven time
tr = 1500
dt = 40
psinl = 0.945
psinr = 1.0075

matplotlib.use('Agg')
case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
timestep = float(case.inp['TIMESTEP'])
dt = floor(float(dt) / timestep)
print('tl_ind={}, tr_ind={}, dt={}'.format(tl, tr, dt))
tind = np.asarray(list(range(tl, tr+1, dt)))
nt = len(tind)
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)
print('xindl={}, xindr={}'.format(xindl, xindr))

Va = case.di['Va']
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Tibar = case.di['Tibar']
Rxy = case.gf['Rxy']
Bpxy = case.gf['Bpxy']
psixy = case.gf['psixy']
nx = case.di.nx
ny = case.di.ny
nz = case.di.nz

Ni = np.zeros([nx, ny, nz, nt])
Ti = Ni.copy()
Vexbx = Ni.copy()
Vbtildx = Ni.copy()
heatflux_par_e = Ni.copy()
heatflux_par_flutter_e = Ni.copy()
print('collecting Ni, Ti, Vexbx, Vbtildx, heatflux_par_e, heat_flux_par_flutter_e : ')
for it in range(nt):
    print('\ttind={}'.format(tind[it]))
    Ni[:, :, :, it] = case.collect('Ni', tind=tind[it], nthreads=1).squeeze()
    Ti[:, :, :, it] = case.collect('Ti', tind=tind[it], nthreads=1).squeeze()
    Vexbx[:, :, :, it] = case.collect('Vexbx', tind=tind[it], nthreads=1).squeeze()
    Vbtildx[:, :, :, it] = case.collect('Vbtildx', tind=tind[it], nthreads=1).squeeze()
    heatflux_par_e[:, :, :, it] = case.collect('heatflux_par_e', tind=tind[it], nthreads=1).squeeze()
    heatflux_par_flutter_e[:, :, :, it] = case.collect('heatflux_par_flutter_e',
                                                  tind=tind[it], nthreads=1).squeeze()
print('collecting N0, Ti0 :')
Ni0 = case.collect('N0', nthreads=1)
Ti0 = case.collect('Ti0', nthreads=1)

# the radial component is determined solely by the contravariant x component
Vexbr = Vexbx / np.reshape((Rxy/Lbar) * (Bpxy/Bbar), (nx, ny, 1, 1))
Vbtildr = Vbtildx / np.reshape((Rxy/Lbar) * (Bpxy/Bbar), (nx, ny, 1, 1))

# Ti gradient, calculated with SI units
drdpsi = 1 / (Rxy * Bpxy)
dpsi = np.zeros([nx, ny])
for iy in range(ny):
    dpsi[:, iy] = np.gradient(psixy[:, iy])
dr = drdpsi * dpsi
rxy = integrate.cumtrapz(dr, axis=0, initial=0)
dTidr = np.zeros([nx, ny])
for iy in range(ny):
    dTidr[:, iy] = np.gradient(Ti0[:, iy] * Tibar, rxy[:, iy])
dTidr_surface_avg = case.gf.surface_avg(dTidr)
dTidr_surface_avg = dTidr_surface_avg[xindl: xindr+1]

# electrostatic radial Ti flux driven by EXB velocity.
# Ti0 is neglected since we do not have equilibrium radial EXB flow, <Ti0*Vexbr>=0
Tirflux_es = Ti * Vexbr * (Tibar * Va)
# electromagnetic radial Ti flux driven by heat conduction along perturbed field line
qer_em = 1.5 * (heatflux_par_e + heatflux_par_flutter_e) * Vbtildr
Tirflux_em = qer_em / (Ni + np.reshape(Ni0, (nx, ny, 1, 1))) * (Tibar * Va)
Tirflux_es_surface_avg = case.gf.surface_avg(Tirflux_es)
Tirflux_em_surface_avg = case.gf.surface_avg(Tirflux_em)
Tirflux_es_surface_avg = Tirflux_es_surface_avg[xindl: xindr+1, :]
Tirflux_em_surface_avg = Tirflux_em_surface_avg[xindl: xindr+1, :]

t = case.t_array[tind]
fig, ax = plt.subplots(3, 2, figsize=[12, 18], facecolor='white')
fig.subplots_adjust(hspace=0.4, wspace=0.6)
ax = ax.flatten()

# ax[0].grid(True)
ct0 = ax[0].contourf(t, psin[xindl: xindr+1], Tirflux_es_surface_avg, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct0, ax=ax[0], fraction=0.08, aspect=30)
ax[0].set_xlabel(r'$t\ (\tau_A)$')
ax[0].set_ylabel(r'$\psi_n$')
ax[0].set_title(r'$\left<\Gamma_{T_e,r,es}\right>_{surf}\ (eVm/s)$')

# ax[1].grid(True)
ct1 = ax[1].contourf(t, psin[xindl: xindr+1], Tirflux_em_surface_avg, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct1, ax=ax[1], fraction=0.08, aspect=30)
ax[1].set_xlabel(r'$t\ (\tau_A)$')
ax[1].set_ylabel(r'$\psi_n$')
ax[1].set_title(r'$\left<\Gamma_{T_e,r,em}\right>_{surf}\ (eVm/s)$')

Tirflux_tot_surface_avg = Tirflux_es_surface_avg + Tirflux_em_surface_avg
# ax[2].grid(True)
ct2 = ax[2].contourf(t, psin[xindl: xindr+1], Tirflux_tot_surface_avg, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct2, ax=ax[2], fraction=0.08, aspect=30)
ax[2].set_xlabel(r'$t\ (\tau_A)$')
ax[2].set_ylabel(r'$\psi_n$')
ax[2].set_title(r'$\left<\Gamma_{T_e,r,tot}\right>_{surf}\ (eVm/s)$')

chi = -Tirflux_tot_surface_avg / np.reshape(dTidr_surface_avg, (xindr - xindl + 1, 1))
# ax[3].grid(True)
ct3 = ax[3].contourf(t, psin[xindl: xindr+1], chi, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct3, ax=ax[3], fraction=0.08, aspect=30)
ax[3].set_xlabel(r'$t\ (\tau_A)$')
ax[3].set_ylabel(r'$\psi_n$')
ax[3].set_title(r'$\chi_e\ (m^2/s)$')

Tirflux_tot_tavg = np.mean(Tirflux_tot_surface_avg, axis=1)
ax[4].plot(psin[xindl: xindr+1], Tirflux_tot_tavg, 'r-')
ax[4].set_xlabel(r'$\psi_n$')
ax[4].set_ylabel(r'$\left<\Gamma_{T_e,r,tot}\right>_t\ (eVm/s)$')
ax[4].grid(True)

chi_tavg = np.mean(chi, axis=1)
ax[5].plot(psin[xindl: xindr+1], chi_tavg, 'b-')
ax[5].set_xlabel(r'$\psi_n$')
ax[5].set_ylabel(r'$\left<\chi_e\right>_t\ (m^2/s)$')
ax[5].grid(True)

outname = os.path.join(case.pert_path,
                         'Tiflux_t{:04d}-{:04d}'.format(int(case.get_time(tl)), int(case.get_time(tr))))
print('Figures have been written to ' + outname)
fig.savefig(outname)
