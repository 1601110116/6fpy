from boutpy.boutdata import Case
from boutpy.boutdata import collect

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
#case_path = '/vol6/home/chaodong/BoutCases/2021.09/nonlinear'
tl = 755  # in Alfven time
tr = 2000
dt = 5
psinl = 0.945
psinr = 1.0075

matplotlib.use('Agg')
case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
timestep = float(case.inp['TIMESTEP'])
dt = floor(float(dt) / timestep)
print('tl_ind={}, tr_ind={}, dt={}'.format(tl, tr, dt))
tind = np.asarray(list(range(tl, tr+1, dt)))
nt = len(tind)
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)
print('xindl={}, xindr={}'.format(xindl, xindr))

Va = case.di['Va']
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Nbar = case.di['Nbar']
density = case.di['density']
Rxy = case.gf['Rxy']
Bpxy = case.gf['Bpxy']
psixy = case.gf['psixy']
nx = case.di.nx
ny = case.di.ny
nz = case.di.nz

print('collecting Ni and Vexbx : ')
# Ni = np.zeros([nx, ny, nz, nt])
# Vexbx = Ni.copy()
# for it in range(nt):
#     print('\ttind={}'.format(tind[it]))
#     Ni[:, :, :, it] = case.collect('Ni',  tind=tind[it], nthreads=1).squeeze()
#     Vexbx[:, :, :, it] = case.collect('Vexbx', tind=tind[it], nthreads=1).squeeze()
Ni = case.collect('Ni', tind=[tl, tr], nthreads=1)
Vexbx = case.collect('Vexbx', tind=[tl, tr], nthreads=1)
print('collecting N0 :')
Ni0 = collect('N0', path=case.data_path, nthreads=1)
Ni_tot = Ni + np.reshape(Ni0, (nx, ny, 1, 1))
del Ni

# the radial component is determined solely by the contravariant x component
Vexbr = Vexbx / np.reshape((Rxy/Lbar) * (Bpxy/Bbar), (nx, ny, 1, 1))
del Vexbx
# Vexbr = np.ones_like(Ni)

# Ni gradient, calculated with SI units
dNidpsi = np.zeros((nx, ny))
for iy in range(ny):
    dNidpsi[:, iy] = np.gradient(Ni0[:, iy] * (Nbar * density / 1e20), psixy[:, iy])
gradNi = dNidpsi * Rxy * Bpxy
# drdpsi = 1 / (Rxy * Bpxy)
# dpsi = np.zeros([nx, ny])
# for iy in range(ny):
#     dpsi[:, iy] = np.gradient(psixy[:, iy])
# dr = drdpsi * dpsi
# rxy = integrate.cumtrapz(dr, axis=0, initial=0)
# dNidr = np.zeros([nx, ny])
# for iy in range(ny):
#     dNidr[:, iy] = np.gradient(Ni0[:, iy] * (Nbar * density / 1e20), rxy[:, iy])
# dNidr_surface_avg = case.gf.surface_avg(dNidr)
# dNidr_surface_avg = dNidr_surface_avg[xindl: xindr+1]
gradNi_surface_avg = case.gf.surface_avg(gradNi)
gradNi_surface_avg = gradNi_surface_avg[xindl: xindr+1]

# We only include electrostatic Ni flux for now, since we assume Vi=0
# Ni0 is neglected since we do not have equilibrium radial EXB flow, <Ni0*Vexbr>=0
Nirflux_es = Ni_tot * Vexbr * (Nbar * density/1e20 * Va)
Nirflux_es_surface_avg = case.gf.surface_avg(Nirflux_es)
Nirflux_es_surface_avg = Nirflux_es_surface_avg[xindl: xindr+1, :]

t = case.t_array[tind]
fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
# Di = -Nirflux_es_surface_avg / np.reshape(dNidr_surface_avg, (xindr - xindl + 1, 1))
Di = -Nirflux_es_surface_avg / np.reshape(gradNi_surface_avg, (xindr - xindl + 1, 1))

Nirflux_es_tavg = np.mean(Nirflux_es_surface_avg, axis=1)
ax.plot(psin[xindl: xindr+1], Nirflux_es_tavg, 'r-')
ax.set_xlabel(r'$\psi_n$')
ax.set_ylabel(r'$\left<\Gamma_{r}\right>\ (\mathrm{10^{20}m^{-3}m/s})$')
ax.grid(True)
xlim = ax.get_xlim()
ylim = ax.get_ylim()
# ax.text(x=xlim[0], y=ylim[1], s='(a)', va='top', ha='left')
outname1 = os.path.join(case.pert_path, 'particle_flux_Gamma_t{:04d}-{:04d}_mth2_newsurf'.format(int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname1 + '.png')
fig.savefig(outname1 + '.eps')
print('Figures have been written to ' + outname1)

fig2, ax2 = plt.subplots(figsize=[6, 6], facecolor='white')
Di_tavg = np.mean(Di, axis=1)
ax2.plot(psin[xindl: xindr+1], Di_tavg, 'b-')
ax2.set_xlabel(r'$\psi_n$')
ax2.set_ylabel(r'$\left<D_i\right>\ (\mathrm{m^2/s})$')
ax2.grid(True)
xlim = ax2.get_xlim()
ylim = ax2.get_ylim()
# ax2.text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left')

outname2 = os.path.join(case.pert_path, 'particle_flux_Di_t{:04d}-{:04d}_mth2_newsurf'.format(int(case.get_time(tl)), int(case.get_time(tr))))
fig2.savefig(outname2 + '.png')
fig2.savefig(outname2 + '.eps')
print('Figures have been written to ' + outname2)
