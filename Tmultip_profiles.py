from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os
import matplotlib

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 10,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)
cases = ['/vol6/home/chaodong/BoutCases/2020.12/Rinnustar',
         '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar_Tscan/T1.1x',
         '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar_Tscan/T1.2x'
]
labels = [r'1.0$\times$',
          r'1.1$\times$',
          r'1.2$\times$']

psinl = 0.9
psinr = 1.05

matplotlib.use('Agg')

fig, ax = plt.subplots(1, 2, figsize=[12, 6], facecolor='w')
fig.subplots_adjust(wspace=0.5)
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel(r'$T_{e0}\ \mathrm{\left(eV\right)}$')
ax[0].grid(True)
ax[1].set_xlabel(r'$\psi_n$')
ax[1].set_ylabel(r'$E_{r0}\ (\mathrm{kV/m})$')
ax[1].grid(True)
for icase in range(len(cases)):
    case = Case(cases[icase])
    xindl, psinl = case.gf.get_xind(psinl)
    xindr, psinr = case.gf.get_xind(psinr)
    yind_omp = case.gf.yind_omp
    psin = case.gf.get_psin(yind='omp')
    psin = psin[xindl: xindr + 1]
    Te0 = case.collect('Te0', xind=[xindl, xindr], yind=yind_omp, nthreads=1).squeeze()
    _, Tenorm = case.di.get_normalization('Te', value=True)
    Te0 *= Tenorm
    ax[0].plot(psin, Te0, label=labels[icase])
    Er0_x = case.collect('Er0_x', yind=yind_omp, nthreads=1).squeeze()
    Rxy = case.gf['Rxy'][:, yind_omp]
    Zxy = case.gf['Zxy'][:, yind_omp]
    Bpxy = case.gf['Bpxy'][:, yind_omp]
    Lbar = case.di['Lbar']
    Bbar = case.di['Bbar']
    Va = case.di['Va']
    Er0 = Er0_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)
    # plot in the unit of kV/m
    Er0 = Er0[xindl: xindr + 1] * 1e-3
    ax[1].plot(psin, Er0, label=labels[icase])


xlim = ax[0].get_xlim()
ylim = ax[0].get_ylim()
ax[0].text(x=xlim[0], y=ylim[1], s='(a)', va='top', ha='left')
ax[0].legend()
xlim = ax[1].get_xlim()
ylim = ax[1].get_ylim()
ax[1].text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left')
ax[1].legend()



outname = os.path.join(case.pert_path,
                       'Tmultip_profiles_psin{:.4f}-{:.4f}'.format(psinl,
                                                                          psinr))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
