from boutpy.boutdata import Case
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'


matplotlib.use('Agg')
case = Case(case_path)
psin, _, yind_omp = case.gf.get_psin(yind='omp', index=True)
Te0 = case.collect('Te0', yind=yind_omp, nthreads=1)
Tebar = case.di['Tebar']
Te0 = Te0 * Tebar
N0 = case.collect('N0', yind=yind_omp, nthreads=1)
Nbar = case.di['Nbar']
density = case.di['density']
N0 = N0 * Nbar * density / 1e20
Rxy = case.gf['Rxy']
Rx = Rxy[:, yind_omp].squeeze()

fig, ax = plt.subplots(1, 2, figsize=[12, 6], facecolor='white')
fig.subplots_adjust(hspace=0.4, wspace=0.6)

ax[0].plot(Rx, Te0)
ax[0].set_xlabel('R (m)')
ax[0].set_ylabel('Te (eV)')
xind_95, _ = case.gf.get_xind(0.95)
xind_1, _ = case.gf.get_xind(1)
print('dr = {} m'.format(Rx[xind_1] - Rx[xind_95]))
print('dTe = {} eV'.format(Te0[xind_95]-Te0[xind_1]))
print('N_95 = {}e20 m^-3'.format(N0[xind_95]))
ax[0].axvline(x=Rx[xind_95])
ax[0].axvline(x=Rx[xind_1])

ax[1].plot(Rx, N0)
ax[1].set_xlabel('R (m)')
ax[1].set_ylabel('N0 (1e20 m^{-3}')
ax[1].axvline(x=Rx[xind_95])
ax[1].axvline(x=Rx[xind_1])
outname = os.path.join(case.eq_path, 'R_profiles')
fig.savefig(outname)
print('Figures have been written to ' + outname)
