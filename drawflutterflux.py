# coding: utf-8
#!/usr/bin/env python
import boutpy.boutdata as bd
import boutpy.boututils as bu
import numpy as np
import scipy.integrate as si
def drawflutterflux(datapath,gridpath,var=np.array([0.0]),varname='pi',tind=[0,1],zind=[0,63]):
     
    grid1=bd.boutgrid(gridpath)
    data1=bu.file_import(datapath+'/BOUT.dmp.0.nc')
    if tind==[0,1]:
       tind[1]=data1['Ppar_i'].shape[0]
   #    tind=[0,int(data1['t_array'][-1])+1]

    if zind==[0,1]:
       zind=[0,data1['MZ']-2]
    Psi=bd.collect('Psi',path=datapath,tind=tind,zind=zind)
    datashape=Psi.shape
    if  var.all()==0:    
        if varname=='pi':
            #pi01=bd.collect('Pperp_i',path=datapath,tind=tind,zind=zind)
            #pi00=bd.collect('Pi0',path=datapath,tind=tind,zind=zind)
            #pi02=bd.collect('Ppar_i',path=datapath,tind=tind,zind=zind)
            #datashape=pi01.shape
            #pi000=np.tile(pi00,(1,datashape[2]*datashape[3]))
            #pi00=pi000.reshape(datashape)
            #pi=pi00+pi01+pi02
            Qi1=bd.collect('Qperp_i',path=datapath,tind=tind,zind=zind)
            Qi2=bd.collect('Qpar_i',path=datapath,tind=tind,zind=zind)
            Qi=Qi1+Qi2 
        elif varname=='pe':
            #pi01=bd.collect('Pperp_e',path=datapath,tind=tind,zind=zind)
            #pdatashape=pi01.shape
            #pi00=bd.collect('Pe0',path=datapath,tind=tind,zind=zind)
            #pi02=bd.collect('Ppar_e',path=datapath,tind=tind,zind=zind)
            #datashape=pi02.shape
            #pi00=np.tile(pi00,(1,datashape[2]*datashape[3]))
            #pi000=np.reshape(pi00,(datashape))
            #pi00=pi000.reshape(datashape)
            #pi=pi00+pi01+pi02
            Qi1=bd.collect('Qperp_e',path=datapath,tind=tind,zind=zind)
            Qi2=bd.collect('Qpar_e',path=datapath,tind=tind,zind=zind)
            #Qi=Qi1+Qi2 
        elif varname == 'ni':
            pi01=bd.collect('ni',path=datapath,tind=tind,zind=zind)
            pi00=bd.collect('ni0',path=datapath,tind=tind,zind=zind)
            datashape=pi01.shape
            pi000=np.tile(pi00,(1,datashape[2]*datashape[3]))
            pi00=pi000.reshape(datashape)
            pi=pi00+pi01
        elif varname == 'ne':
            pi01=bd.collect('ne',path=datapath,tind=tind,zind=zind)
            pi00=bd.collect('ne0',path=datapath,tind=tind,zind=zind)
            datashape=pi01.shape
            pi000=np.tile(pi00,(1,datashape[2]*datashape[3]))
            pi00=pi000.reshape(datashape)
            pi=pi00+pi01
    else:
        Qi=var
    b=grid1['Bxy']
    bp=grid1['Bpxy']
    bp=np.tile(bp,(1,datashape[2]*datashape[3]))
    bp=bp.reshape(datashape)
    b=np.tile(b,(1,datashape[2]*datashape[3]))
    b=b.reshape(datashape)
    R=grid1['Rxy']
    R=R.reshape(datashape[0],datashape[1],1,1)
    R=np.tile(R,(1,1,datashape[2],datashape[3]))
    dz=np.linspace(2*np.pi*zind[0]/64,2*np.pi*zind[1]/64,num=zind[1]-zind[0]+1)
    gradzPsi=np.gradient(Psi,1,1,dz,1)
    pfluxflutter=(bp/b*gradzPsi[2]/b/R)
    pfluxflutter=(Qi1+Qi2)*pfluxflutter*grid1['rmag']
    return pfluxflutter
