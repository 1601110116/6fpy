from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-8_hd4parapar1e-6'
time = 1500  # in Alfven time
yind = 76
psinl = 0.93
psinr = 1.02

case = Case(case_path)
tind = case.get_tstep(time)
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)

Ni = case.collect('Ni', yind=yind, tind=tind, nthreads=1).squeeze()
Nirms = Ni.std(axis=-1)
_, Ninorm = case.di.get_normalization('Ni', value=True)
Nirms *= Ninorm

Er0_x = case.collect('Er0_x', yind=yind, nthreads=1).squeeze()
Rxy = case.gf['Rxy'][:, yind]
Zxy = case.gf['Zxy'][:, yind]
Bpxy = case.gf['Bpxy'][:, yind]
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Va = case.di['Va']
Er0 = Er0_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)

fig, ax = plt.subplots(figsize=[6, 6], facecolor='w')
ax.grid(True)
ax.tick_params(axis='y', labelcolor='b')
ax.plot(psin[xindl: xindr+1], Er0[xindl: xindr+1], 'b-')
ax.set_xlabel(r'$\psi_n$')
ax.set_ylabel(r'$E_r\ \left(\mathrm{V/m}\right)$')

ax_Nirms = ax.twinx()
ax_Nirms.grid(True)
ax_Nirms.tick_params(axis='y', labelcolor='r')
ax_Nirms.plot(psin[xindl: xindr+1], Nirms[xindl: xindr+1], 'r-')
ax_Nirms.set_ylabel(r'$n_{i,rms}\ \left(\mathrm{10^{{20}}\ m^{{-3}}}\right)$')

outname = os.path.join(case.pert_path,
                       'Nirms_Er_t{:04d}y{:03d}.png'.format(int(case.get_time(tind)), yind))
fig.savefig(outname)
print('Figures have been written to ' + outname)
