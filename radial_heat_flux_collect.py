from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os
import tracemalloc

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
tl = 750  # in Alfven time
tr = 760
psinl = 0.945
psinr = 1.01
SI = True

matplotlib.use('Agg')
case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
print('tl_ind={}, tr_ind={}'.format(tl, tr))
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)
print('xindl={}, xindr={}'.format(xindl, xindr))

Va = case.di['Va']
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Tebar = case.di['Tebar']
Tibar = case.di['Tibar']
Nbar = case.di['Nbar']
density = case.di['density']
Rxy = case.gf['Rxy']
Bpxy = case.gf['Bpxy']
psixy = case.gf['psixy']
nx = case.di.nx
ny = case.di.ny
nz = case.di.nz

tind = np.asarray(list(range(tl, tr + 1)))
nt = len(tind)

print('collecting N0, Te0, Ti0 and B0')
Ni0 = case.collect('N0', nthreads=1)
Te0 = case.collect('Te0', nthreads=1)
Ti0 = case.collect('Ti0', nthreads=1)
B0 = case.collect('B0',nthreads=1)

print('calculating gradients')
gradpsi = Rxy * Bpxy

dTedpsi = np.zeros((nx, ny))
for iy in range(ny):
    dTedpsi[:, iy] = np.gradient(Te0[:, iy] * Tebar, psixy[:, iy])
gradTe = dTedpsi * gradpsi
ngradTe = Nbar * density / 1e20 * Ni0 * gradTe
ngradTe_surface_avg = case.gf.surface_avg(ngradTe)
ngradTe_surface_avg = ngradTe_surface_avg[xindl: xindr + 1]

dTidpsi = np.zeros((nx, ny))
for iy in range(ny):
    dTidpsi[:, iy] = np.gradient(Ti0[:, iy] * Tibar, psixy[:, iy])
gradTi = dTidpsi * gradpsi
ngradTi = Nbar * density / 1e20 * Ni0 * gradTi
ngradTi_surface_avg = case.gf.surface_avg(ngradTi)
ngradTi_surface_avg = ngradTi_surface_avg[xindl: xindr + 1]

heatflux_e_es_tavg = np.zeros(xindr - xindl + 1)
heatflux_i_es_tavg = np.zeros(xindr - xindl + 1)
heatflux_e_em_tavg = np.zeros(xindr - xindl + 1)
heatflux_i_em_tavg = np.zeros(xindr - xindl + 1)

tracemalloc.start()
for it in range(nt):
    print('step {} of {}'.format(it, nt))
    print('collecting Ni, Te, Ti and Vexbx')
    Ni = case.collect('Ni', tind=tind[it], nthreads=1).squeeze()
    size1, peak1 = tracemalloc.get_traced_memory()
    print('memory1: {:>10.4f} MB'.format(peak1 / 1024 / 1024))
    Te = case.collect('Te', tind=tind[it], nthreads=1).squeeze()
    size2, peak2 = tracemalloc.get_traced_memory()
    print('memory2: {:>10.4f} MB'.format(peak2 / 1024 / 1024))
    Ti = case.collect('Ti', tind=tind[it], nthreads=1).squeeze()
    Ni_tot = Ni + np.reshape(Ni0, (nx, ny, 1))
    Te_tot = Te + np.reshape(Te0, (nx, ny, 1))
    Ti_tot = Ti + np.reshape(Ti0, (nx, ny, 1))
    Vexbx = case.collect('Vexbx', tind=tind[it], nthreads=1).squeeze()
    Vexbr = Vexbx / np.reshape((Rxy / Lbar) * (Bpxy / Bbar), (nx, ny, 1))
    heatflux_e_es = Ni_tot * Te_tot * Vexbr * (Nbar * density / 1e20 * Tebar * Va)
    heatflux_e_es_surface_avg = case.gf.surface_avg(heatflux_e_es)
    heatflux_e_es_tavg = heatflux_e_es_tavg + heatflux_e_es_surface_avg[xindl: xindr + 1]
    heatflux_i_es = Ni_tot * Ti_tot * Vexbr * (Nbar * density / 1e20 * Tibar * Va)
    heatflux_i_es_surface_avg = case.gf.surface_avg(heatflux_i_es)
    heatflux_i_es_tavg = heatflux_i_es_tavg + heatflux_i_es_surface_avg[xindl: xindr + 1]

    print('collecting Vbtildx, heatflux_par_e, heatflux_par_flutter_e, heatflux_par_i and heatflux_par_flutter_i')
    size3, peak3 = tracemalloc.get_traced_memory()
    print('memory2: {:>10.4f} MB'.format(peak3 / 1024 / 1024))
    Vbtildx = case.collect('Vbtildx', tind=tind[it], nthreads=1).squeeze()
    Vbtildx = Vbtildx / np.reshape(B0, (nx, ny, 1))
    Vbtildr = Vbtildx / np.reshape((Rxy/Lbar) * (Bpxy/Bbar), (nx, ny, 1))
    heatflux_par_e = case.collect('heatflux_par_e', tind=tind[it], nthreads=1).squeeze()
    heatflux_par_flutter_e = case.collect('heatflux_par_flutter_e', tind=tind[it], nthreads=1).squeeze()
    qer_em = 1.5 * (heatflux_par_e + heatflux_par_flutter_e) * Vbtildr
    heatflux_e_em = qer_em * (Nbar * density/1e20 * Tebar * Va)
    heatflux_e_em_surface_avg = case.gf.surface_avg(heatflux_e_em)
    heatflux_e_em_tavg = heatflux_e_em_tavg + heatflux_e_em_surface_avg[xindl: xindr + 1]

    heatflux_par_i = case.collect('heatflux_par_i', tind=tind[it], nthreads=1).squeeze()
    heatflux_par_flutter_i = case.collect('heatflux_par_flutter_i', tind=tind[it], nthreads=1).squeeze()
    qir_em = 1.5 * (heatflux_par_i + heatflux_par_flutter_i) * Vbtildr
    heatflux_i_em = qir_em * (Nbar * density/1e20 * Tibar * Va)
    heatflux_i_em_surface_avg = case.gf.surface_avg(heatflux_i_em)
    heatflux_i_em_tavg = heatflux_i_em_tavg + heatflux_i_em_surface_avg[xindl: xindr + 1]
    size, peak = tracemalloc.get_traced_memory()
    print('memory: {:>10.4f} MB'.format(peak / 1024 / 1024))
    del Ni, Te, Ti, Ni_tot, Te_tot, Ti_tot, Vexbx, Vexbr, heatflux_e_es
    del heatflux_e_es_surface_avg, heatflux_i_es, heatflux_i_es_surface_avg
    del Vbtildx, Vbtildr, heatflux_par_e, heatflux_par_flutter_e
    del qer_em, heatflux_e_em, heatflux_e_em_surface_avg, heatflux_par_i
    del heatflux_par_flutter_i, qir_em, heatflux_i_em, heatflux_i_em_surface_avg
    print()

heatflux_e_es_tavg = heatflux_e_es_tavg / nt
heatflux_i_es_tavg = heatflux_i_es_tavg / nt
heatflux_e_em_tavg = heatflux_e_em_tavg / nt
heatflux_i_em_tavg = heatflux_i_em_tavg / nt
timel = int(case.get_time(tl))
timer = int(case.get_time(tr))
data_file_name = os.path.join(case.pert_path, 'radial_heat_flux_t{:04d}-{:04d}.npz'.format(timel, timer))
np.savez(data_file_name,
         xindl=xindl, xindr=xindr,
         ngradTe_surface_avg=ngradTe_surface_avg,
         ngradTi_surface_avg=ngradTi_surface_avg,
         heatflux_e_es_tavg=heatflux_e_es_tavg,
         heatflux_i_es_tavg=heatflux_i_es_tavg,
         heatflux_e_em_tavg=heatflux_e_em_tavg,
         heatflux_i_em_tavg=heatflux_i_em_tavg)
# with open(data_file_name, 'wb') as data_file:
#     np.save(data_file, xindl)
#     np.save(data_file, xindr)
#     np.save(data_file, ngradTe_surface_avg)
#     np.save(data_file, ngradTi_surface_avg)
#     np.save(data_file, heatflux_e_es_tavg)
#     np.save(data_file, heatflux_i_es_tavg)
#     np.save(data_file, heatflux_e_em_tavg)
#     np.save(data_file, heatflux_i_em_tavg)
# pass
