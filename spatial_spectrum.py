from boutpy.boutdata import Case

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-8_hd4parapar1e-6'
time = 1500  # in Alfven time
psin = 0.988
yind = 76
var_name = 'Ni'
# analyse up to this harmonic. If None, use the maximum harmonic included in the case
max_harmonic = None

case = Case(case_path)
tind = case.get_tstep(time)
xind, _ = case.gf.get_xind(psin)
low_pass_z = int(case.inp['highbeta']['low_pass_z'])
if max_harmonic is None:
    max_harmonic = low_pass_z
if max_harmonic > low_pass_z:
    max_harmonic = low_pass_z

var = case.collect(var_name, xind, yind, tind=tind, nthreads=1).squeeze()
varn = np.fft.rfft(var, axis=0)
rmsn = (1 / np.sqrt(2)) * np.abs(varn) / (case.di.nz / 2)
rmsn = rmsn[:max_harmonic+1]
_, norm = case.di.get_normalization(var_name, value=True)
rmsn *= norm

zperiod = int(case.inp['ZPERIOD'])
R = case.gf['Rxy'][xind, yind]
Bp = case.gf['Bpxy'][xind, yind]
Bt = case.gf['Btxy'][xind, yind]

n = np.arange(max_harmonic + 1) * zperiod
# poloidal wave number in cm^-1
kthe = n / R * Bt / Bp / 100

fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
ax.grid(True)
ax_kthe = ax.twiny()
ax.plot(n, rmsn, 'b-')
ax_kthe.plot(kthe, rmsn, 'b-')
ax.set_xlabel('n')
ax_kthe.set_xlabel(r'$k_{\theta}\ \left(\mathrm{cm^{-1}}\right)$')
ax.set_ylabel(r'$n_{rms}\ \left(\mathrm{10^{{20}}\ m^{{-3}}}\right)$')
ax.legend(title=r't={} $\tau_A$'.format(int(case.get_time(tind))), loc='upper left')

outname = os.path.join(case.pert_path,
                       'spatial_spectrum_x{:04d}y{:03d}t{:04d}.png'.format(xind, yind, int(case.get_time(tind))))
fig.savefig(outname)
print('Figures have been written to ' + outname)


