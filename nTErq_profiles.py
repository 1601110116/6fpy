from boutpy.boutdata import Case
from boutpy.boututils import DataFile

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os
import sys


rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
case_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans/n50'
exp_path = '/vol6/home/chaodong/gridfiles/cmod1120907032/flat_q/flat_NT1.01/0.9-1.05nx260ny128_flat_edge_wo_smooth'
pfile_name = 'p1120907032.01010'
CXRS_name: str = "cxrs1120907032.01010_v20140623.txt"
density = 1e20

case = Case(case_path)
psin, yind_imp, yind_omp = case.gf.get_psin(yind='omp', index=True)
sys.path.append(exp_path)
from equilibrium_parsers import PfileParser
pp = PfileParser(os.path.join(exp_path, pfile_name))
psinorm_p = pp.get_psinorm()
Ni_p, Ni_unit_p = pp.get_ni()
Te_p, Te_unit_p = pp.get_te()
psinorm_p = np.asarray(psinorm_p)
Ni_p = np.asarray(Ni_p)
Te_p = np.asarray(Te_p)
Ni_p = Ni_p / density
Te_p = Te_p * 1e3

from equilibrium_parsers import CXRSParser
Cp = CXRSParser(os.path.join(exp_path, CXRS_name))
Er_psinC, ErC, Er_errC, Er_unitC = Cp.get_Er()
Er_psinC = np.asarray(Er_psinC)
ErC = np.asarray(ErC)
ErC = ErC * 1e3

yind = yind_omp
Ni0 = case.collect('N0', yind=yind, nthreads=1).squeeze()
Te0 = case.collect('Te0', yind=yind, nthreads=1).squeeze()
_, Ninorm = case.di.get_normalization('Ni', value=True)
Ni0 *= Ninorm
_, Tenorm = case.di.get_normalization('Te', value=True)
Te0 *= Tenorm
Er0_x = case.collect('Er0_x', yind=yind, nthreads=1).squeeze()
Rxy = case.gf['Rxy'][:, yind]
Zxy = case.gf['Zxy'][:, yind]
Bpxy = case.gf['Bpxy'][:, yind]
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Va = case.di['Va']
Er0 = Er0_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)

q_gfile = np.abs(case.gf.q)
q_sim = np.abs(case.gf['q'])

fig, ax = plt.subplots(2, 2, figsize=[12, 10], facecolor='w', sharex=True)
fig.subplots_adjust(hspace=0.1, wspace=0.4)
ax = ax.flatten()

ax[0].plot(psinorm_p, Ni_p, 'b-', label='exp')
ax[0].plot(psin, Ni0, 'r-', label='sim')
# ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel(r'$n_{i0}\ \left(10^{{20}}\ \mathrm{m^{{-3}}}\right)$')
ax[0].grid(True)
ax[0].legend()
ax[0].set(xlim=(psin[0], psin[-1]), ylim=(0, 1.1*Ni0[0]))
ax[0].text(x=psin[0], y=0, s='(a)', va='bottom', ha='left')

ax[1].plot(psinorm_p, Te_p, 'b-', label='exp')
ax[1].plot(psin, Te0, 'r-', label='sim')
# ax[1].set_xlabel(r'$\psi_n$')
ax[1].set_ylabel(r'$T_{e0}\ (\mathrm{eV})$')
ax[1].grid(True)
ax[1].set(xlim=(psin[0], psin[-1]), ylim=(0, 1.1*Te0[0]))
ax[1].text(x=psin[0], y=0, s='(b)', va='bottom', ha='left')

ax[2].plot(Er_psinC, ErC * 1e-3, 'b-')
ax[2].plot(psin, Er0 * 1e-3, 'r-')
ax[2].set_xlabel(r'$\psi_n$')
ax[2].set_ylabel(r'$E_{r0}\ (\mathrm{kV/m})$')
ax[2].grid(True)
ax[2].set(xlim=(psin[0], psin[-1]))
ylim = ax[2].get_ylim()
ax[2].text(x=psin[0], y=ylim[0], s='(c)', va='bottom', ha='left')

ax[3].plot(psin, q_gfile, 'b-')
ax[3].plot(psin, q_sim[:, yind], 'r-')
ax[3].set(xlim=(psin[0], psin[-1]))
ax[3].set_xlabel(r'$\psi_n$')
ax[3].set_ylabel('q')
ax[3].grid(True)
ylim = (1.5, 9)
ax[3].set(ylim=ylim)
ax[3].text(x=psin[0], y=ylim[0], s='(d)', va='bottom', ha='left')

outname = os.path.join(case.eq_path, 'nTErq')
outname = os.path.join(case.eq_path, 'nTErq')
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)

