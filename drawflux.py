# coding: utf-8
#!/usr/bin/env python
import boutpy.boutdata as bd
import boutpy.boututils as bu
import numpy as np
import scipy.integrate as si
"""
returned variable is [0]flux [1]areas element [2]pressure or density
datapath gridpath are necessary
"""
def drawflux(datapath,gridpath,var=np.array([0.0]),varname='pi',tind=[0,1],zind=[0,63]):
     
    grid1=bd.boutgrid(gridpath)
    data1=bu.file_import(datapath+'/BOUT.dmp.0.nc')
    if tind==[0,1]:
       tind[1]=data1['Ppar_i'].shape[0]
   #    tind=[0,int(data1['t_array'][-1])+1]

    if zind==[0,1]:
       zind=[0,data1['MZ']-2]
    phi=bd.collect('phi',path=datapath,tind=tind,zind=zind)
    datashape=phi.shape
    if  var.all()==0:    
        if varname=='pi':
            pi01=bd.collect('Pperp_i',path=datapath,tind=tind,zind=zind)
            pi00=bd.collect('Pi0',path=datapath,tind=tind,zind=zind)
            pi02=bd.collect('Ppar_i',path=datapath,tind=tind,zind=zind)
            datashape=pi01.shape
            pi000=np.tile(pi00,(1,datashape[2]*datashape[3]))
            pi00=pi000.reshape(datashape)
            pi=pi00+pi01+pi02
    
        elif varname=='pe':
            pi01=bd.collect('Pperp_e',path=datapath,tind=tind,zind=zind)
            pdatashape=pi01.shape
            pi00=bd.collect('Pe0',path=datapath,tind=tind,zind=zind)
            pi02=bd.collect('Ppar_e',path=datapath,tind=tind,zind=zind)
            datashape=pi02.shape
            pi00=np.tile(pi00,(1,datashape[2]*datashape[3]))
            pi000=np.reshape(pi00,(datashape))
            pi00=pi000.reshape(datashape)
            pi=pi00+pi01+pi02
        
        elif varname == 'ni':
            pi01=bd.collect('ni',path=datapath,tind=tind,zind=zind)
            pi00=bd.collect('ni0',path=datapath,tind=tind,zind=zind)
            datashape=pi01.shape
            pi000=np.tile(pi00,(1,datashape[2]*datashape[3]))
            pi00=pi000.reshape(datashape)
            pi=pi00+pi01
        elif varname == 'ne':
            pi01=bd.collect('ne',path=datapath,tind=tind,zind=zind)
            pi00=bd.collect('ne0',path=datapath,tind=tind,zind=zind)
            datashape=pi01.shape
            pi000=np.tile(pi00,(1,datashape[2]*datashape[3]))
            pi00=pi000.reshape(datashape)
            pi=pi00+pi01
    else:
        pi=var
    
    bt=grid1['Btxy']
    bp=grid1['Bpxy']
    bp1=np.tile(bp,(1,datashape[2]*datashape[3]))
    bp=bp1.reshape(datashape)
    bt1=np.tile(bt,(1,datashape[2]*datashape[3]))
    bt=bt1.reshape(datashape)
    r_minor=bd.boutgrid.get_minor_r(grid1)
    r_minor1=r_minor.reshape(datashape[0],1,1,1)
    r_minor=np.tile(r_minor1,(1,datashape[1],datashape[2],datashape[3]))
    q=bd.boutgrid.get_q(grid1)
    q1=q.reshape(datashape[0],1,1,1)
    q=np.tile(q1,(1,datashape[1],datashape[2],datashape[3]))
    R=grid1['Rxy']
    R1=R.reshape(datashape[0],datashape[1],1,1)
    R=np.tile(R1,(1,1,datashape[2],datashape[3]))
    dy=np.linspace(0,2*np.pi,num=datashape[1])
    dz=np.linspace(2*np.pi*zind[0]/64,2*np.pi*zind[1]/64,num=zind[1]-zind[0]+1)
    E=np.gradient(phi,1,dy,dz,2)
    ep=(E[1]-q*E[2])
    ep=ep/r_minor
    et=E[2]/R
    b=(bt**2+bp**2)**0.5
    pflux=pi*(ep*bt-et*bp)/b**2
    
    Bbar=grid1['bmag']# unnormalization
    Lbar=grid1['rmag']
    Mi=2.0*1.6726e-27
    density=1e20
    MU0=4.0e-7*np.pi
    Vbar=np.sqrt(Bbar*Bbar/MU0/Mi/density)
    dtheta=2.*np.pi/float(grid1.ny)
    dl=np.sqrt(np.gradient(grid1['Rxy'],axis=1)**2+np.gradient(grid1['Zxy'],axis=1)**2)*dtheta
    dA=grid1['Bxy']/grid1['Bpxy']*grid1['Rxy']*dl
    A=si.cumtrapz(dA,x=np.arange(grid1.ny),initial=0)
    A=A*2*np.pi
    A=A[:,-1]
  
    if varname=='pe'or varname=='pi':
       pflux=pflux*density*Mi*Vbar*Vbar*Vbar
       
    if varname=='ne'or varname=='ni':
        
       pflux=pflux*density*Vbar
    return pflux,A,pi


