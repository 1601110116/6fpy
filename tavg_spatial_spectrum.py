from boutpy.boutdata import Case

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os
from math import floor

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 10,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.11/Tscan/T1.1x_Erdia_hd4paru1e-6'
# case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-6'
# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
# case_path = '/vol6/home/chaodong/BoutCases/2021.06/tf_P0'
# case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
case_path = '/vol6/home/chaodong/BoutCases/2021.09/nonlinear'
# tl = 1400  # in Alfven time
# tr = 1500
tl = 750  # in Alfven time
tr = 2000
dt = 5
# psin = 0.975
psin = 0.988
yind = 76
var_name = 'Ni'
# analyse up to this harmonic. If None, use the maximum harmonic included in the case
max_harmonic = None

case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
timestep = float(case.inp['TIMESTEP'])
dt = floor(float(dt) / timestep)
print('tl_ind={}, tr_ind={}, dt={}'.format(tl, tr, dt))
tind = np.asarray(list(range(tl, tr+1, dt)))
nt = len(tind)
xind, _ = case.gf.get_xind(psin)
low_pass_z = int(case.inp['highbeta']['low_pass_z'])
if max_harmonic is None:
    max_harmonic = low_pass_z
if max_harmonic > low_pass_z:
    max_harmonic = low_pass_z

nz = case.di.nz
var = np.zeros([nz, nt])
for it in range(nt):
    print('tind = {}'.format(tind[it]))
    var[:, it] = case.collect(var_name, xind, yind, tind=tind[it], nthreads=1).squeeze()
varn = np.fft.rfft(var, axis=0)
rmsn = (1 / np.sqrt(2)) * np.abs(varn) / (case.di.nz / 2)
rmsn = rmsn[:max_harmonic+1, :]
rmsn = rmsn.mean(axis=-1)
_, norm = case.di.get_normalization(var_name, value=True)
rmsn *= norm

zperiod = int(case.inp['ZPERIOD'])
R = case.gf['Rxy'][xind, yind]
Bp = case.gf['Bpxy'][xind, yind]
Bt = case.gf['Btxy'][xind, yind]

n = np.arange(max_harmonic + 1) * zperiod
# poloidal wave number in cm^-1
kthe = n / R * Bt / Bp / 100

fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
ax.grid(True)
ax_kthe = ax.twiny()
ax.plot(n, rmsn, 'b-')
ax_kthe.plot(kthe, rmsn, 'b-o')
ax.set_xlabel('$n$')
ax_kthe.set_xlabel(r'$k_{\theta}\ \left(\mathrm{cm^{-1}}\right)$')
ax.set_ylabel(r'$n_{i1}\ \left(\mathrm{10^{{20}}\ m^{{-3}}}\right)$')
ylim = ax.get_ylim()
# ax.text(x=0, y=ylim[1], s='(b)', va='top', ha='left')
# ax.legend(title=r't={}-{} $\tau_A$'.format(int(case.get_time(tl)), int(case.get_time(tr))), loc='upper left')

outname = os.path.join(case.pert_path,
                       var_name + 'spatial_spectrum_x{:04d}y{:03d}t{:04d}-{:04d}'.format(
                           xind, yind, int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)


