from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
tl = 1500  # in Alfven time
tr = 1515

matplotlib.use('Agg')
case = Case(case_path)
data_file_name = os.path.join(case.pert_path, 'radial_heat_flux_t{:04d}-{:04d}.npz'.format(tl, tr))
print('loading ' + data_file_name + ' ...')
with np.load(data_file_name) as data:
    xindl = data['xindl']
    xindr = data['xindr']
    ngradTe_surface_avg = data['ngradTe_surface_avg']
    ngradTi_surface_avg = data['ngradTi_surface_avg']
    heatflux_e_es_tavg = data['heatflux_e_es_tavg']
    heatflux_i_es_tavg = data['heatflux_i_es_tavg']
    heatflux_e_em_tavg = data['heatflux_e_em_tavg']
    heatflux_i_em_tavg = data['heatflux_i_em_tavg']
heatflux_e_tot = heatflux_e_es_tavg + heatflux_e_em_tavg
heatflux_i_tot = heatflux_i_es_tavg + heatflux_i_em_tavg
psin = case.gf.get_psin(yind='omp')

print('plotting ...')
fig1, ax1 = plt.subplots(figsize=[6, 6], facecolor='white')
ee = 1.6021766208e-19
ax1.plot(psin[xindl: xindr+1], 1e20*ee*1e-6 * heatflux_e_tot, 'b-',
         label=r'$\left<q_{e,r}\right>\ \left(\mathrm{MW/m^2}\right)$')
ax1.plot(psin[xindl: xindr+1], 1e20*ee*1e-6 * heatflux_i_tot, 'r-',
         label=r'$\left<q_{i,r}\right>\ \left(\mathrm{MW/m^2}\right)$')
ax1.set_ylabel('heat flux')
ax1.set_xlabel(r'$\psi_n$')
ax1.grid(True)
ax1.legend()
outname1 = os.path.join(case.pert_path, 'radial_heat_flux_t{:04d}-{:04d}_2'.format(tl, tr))
fig1.savefig(outname1 + '.png')
fig1.savefig(outname1 + '.eps')
print('Figures have been written to ' + outname1)



