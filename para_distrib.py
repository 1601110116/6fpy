from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
import os
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import rcParams

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-8_hd4parapar1e-6'
# case_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans/n50'
# case_path = '/vol6/home/chaodong/BoutCases/2021.09/eHallF_tfp1_n50'
case_path = '/vol6/home/chaodong/BoutCases/2021.09/tf_coeff_scan_n50/tf_coeff0.0'

t = -1
psin = 1.0
yindl = 43
yindr = 119
var_name = 'phi'

matplotlib.use('Agg')
case = Case(case_path)
tind = case.get_tstep(t)
xind, _ = case.gf.get_xind(psin)

ny = case.di.ny
nz = case.di.nz
dy = 2 * np.pi / ny
hthe = case.gf['hthe']
hthe = hthe[xind, :].squeeze()
dlthe = hthe * dy

nxpoint = case.gf.topology()
if nxpoint == 0:
    # radius of curvature
    roc = hthe
elif nxpoint == 1:
    roc = hthe / ny * case.gf['npol'][1]
else:
    raise NotImplementedError
Rxy = case.gf['Rxy']
R = Rxy[xind]
Btxy = case.gf['Btxy']
Bpxy = case.gf['Bpxy']
Bt = Btxy[xind, :].squeeze()
Bp = Bpxy[xind, :].squeeze()
# local pitch angle dthe/dz
nu = (roc * Bt) / (R * Bp)
dlparadlthe = np.sqrt(Bt ** 2 + Bp ** 2) / Bp
dlpara = dlthe * dlparadlthe
lpara = np.cumsum(dlpara)
psi_n, yind_imp, yind_omp = case.gf.get_psin(yind='omp', index=True)
lpara -= lpara[yind_omp]
lpara = lpara[yindl: yindr+1].reshape(yindr-yindl+1, 1)
lpara = np.tile(lpara, (1, nz))

dz = 2 * np.pi / (case.di.zperiod * nz)
z = dz * np.arange(nz).reshape(1, nz)
z = np.tile(z, (yindr - yindl + 1, 1))
var = case.collect(var_name, xind=xind, yind=[yindl, yindr], tind=tind, nthreads=1).squeeze()
vmax = var.max()
vmin = var.min()
if not vmax > vmin:
    vmax += 1
max_abs = np.max([np.abs(vmin), np.abs(vmax)])
vmax = max_abs
vmin = -max_abs
levels = np.linspace(vmin, vmax, 200, endpoint=True)
fig, ax = plt.subplots(figsize=[8, 8], facecolor='w')
ct = ax.contourf(z, lpara, var, cmap=plt.get_cmap('bwr'), levels=levels)
fig.colorbar(ct, ax=ax, fraction=0.08, aspect=30)
ax.set_xlabel('z')
ax.set_ylabel(r'$\mathcal{l}_{\parallel}$ (m)')
ax.set_title(var_name + r' at $\psi_n={:.4f}$'.format(psi_n[xind]))
ax.grid(True)
outname = os.path.join(case.pert_path, 'z-para_' + var_name +
                       '_t{:04d}_x{:03d}_y{:03d}-{:03d}.png'.format(
                           tind, xind, yindl, yindr
                       ))
fig.savefig(outname)
print('Figures have been written to ' + outname)
