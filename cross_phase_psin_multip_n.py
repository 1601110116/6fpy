from boutpy.boutdata import Case
from boutpy.visualization import color_list

import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
import matplotlib

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 16,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
# case_path = '/vol6/home/chaodong/BoutCases/2021.09/nonlinear'
# case_path = '/vol6/home/chaodong/BoutCases/2021.09/eHallF_tfF_n50'
# case_path = '/vol6/home/chaodong/BoutCases/2021.06/tf_sptz_resist'

var1_name = 'Ni'
var2_name = 'phi'
# psin = 0.975
psinl = 0.945
psinr = 1.0075
yind = 76
tl = 1080
tr = 1080
# tl = 800
# tr = 800
# tl = 300
# tr = 300
cmap = 'coolwarm'
# harmonics = list(range(1, 2))  # must be a list object
harmonics = list(range(6, 11))
phase_min = -2  # cross_phase is refined in the range (phase_min, phase_min+2*pi)

matplotlib.use('Agg')
case = Case(case_path)
xindl, psinl = case.gf.get_xind(psinl)
xindr, psinr = case.gf.get_xind(psinr)
zperiod = int(case.inp['ZPERIOD'])
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
t = case.t_array[tl: tr + 1]

# specify the axis of squeeze() in case only one time step is collected
var1 = case.collect(var1_name, xind=[xindl, xindr], yind=yind, tind=[tl, tr], nthreads=1).squeeze(axis=1)
var2 = case.collect(var2_name, xind=[xindl, xindr], yind=yind, tind=[tl, tr], nthreads=1).squeeze(axis=1)

var1_k = np.fft.rfft(var1, axis=-2)
var2_k = np.fft.rfft(var2, axis=-2)
var1_k = var1_k[:, harmonics, :]
var2_k = var2_k[:, harmonics, :]

# this is \vartheta_{var2} - \vartheta_{var1} in the form e^{-in\zeta+ivartheta}
cross_phase = np.angle(var1_k / var2_k)
cross_phase[cross_phase < phase_min] += 2*np.pi
var1_rms = (1 / np.sqrt(2)) * np.abs(var1_k) / (case.di.nz / 2)
var2_rms = (1 / np.sqrt(2)) * np.abs(var2_k) / (case.di.nz / 2)
# time average
cross_phase = np.mean(cross_phase, axis=-1)
var1_rms = np.mean(var1_rms, axis=-1)
var2_rms = np.mean(var2_rms, axis=-1)

psin = case.gf.get_psin(yind='omp')
psin = psin[xindl: xindr+1]
_, var1_norm = case.di.get_normalization(var1_name, value=True)
_, var2_norm = case.di.get_normalization(var2_name, value=True)
var1_latex, var1_unit = case.di.get_varname_latex(var1_name)
var2_latex, var2_unit = case.di.get_varname_latex(var2_name)
fig, ax = plt.subplots(3, 1, figsize=[10, 15], facecolor='white', sharex='col')
nharmonics = len(harmonics)
ax[0].set_prop_cycle(color=color_list(nharmonics, cmap=cmap))
ax[1].set_prop_cycle(color=color_list(nharmonics, cmap=cmap))
ax[2].set_prop_cycle(color=color_list(nharmonics, cmap=cmap))
for iharmonic in range(nharmonics):
    ax[0].plot(psin, var1_norm * var1_rms[:, iharmonic], label='n={:02d}'.format(harmonics[iharmonic]*zperiod))
    ax[1].plot(psin, var2_norm * var2_rms[:, iharmonic], label='n={:02d}'.format(harmonics[iharmonic]*zperiod))
    ax[2].plot(psin, cross_phase[:, iharmonic], label='n={:02d}'.format(harmonics[iharmonic]*zperiod))
ax[2].set_xlabel(r'$\psi_n$')

ax[0].set_ylabel(r'$\left<{}\right>_{{rms}}\ \left(\mathrm{{{}}}\right)$'.format(var1_latex, var1_unit))
ax[1].set_ylabel(r'$\left<{}\right>_{{rms}}\ \left(\mathrm{{{}}}\right)$'.format(var2_latex, var2_unit))
ax[2].set_ylabel(r'$\vartheta_{{{}}}-\vartheta_{{{}}}$'.format(var2_latex, var1_latex))
ax[0].legend(title='toroidal mode number')
ax[0].grid(True)
ax[1].grid(True)
ax[2].grid(True)
xlim = ax[0].get_xlim()
ylim = ax[0].get_ylim()
y_txt = (ylim[0] + ylim[1]) / 2
ax[0].text(x=xlim[0], y=y_txt, s='(a)', va='top', ha='left')
xlim = ax[1].get_xlim()
ylim = ax[1].get_ylim()
y_txt = (ylim[0] + ylim[1]) / 2
ax[1].text(x=xlim[0], y=y_txt, s='(b)', va='top', ha='left')
xlim = ax[2].get_xlim()
ylim = ax[2].get_ylim()
y_txt = (ylim[0] + ylim[1]) / 2
ax[2].text(x=xlim[0], y=y_txt, s='(c)', va='top', ha='left')
# xlim = ax.get_xlim()
# ylim = ax.get_ylim()
# ax.text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left')

harmonics_str = '{}'.format(harmonics).strip('[]').replace(', ', '_')
outname = os.path.join(
    case.pert_path, 'cross_phase_h' + harmonics_str + '_' + var1_name + '-' + var2_name +
                    '_psin{:.4f}-{:.4f}_y{:03d}_t{:04d}-{:04d}'.format(
                        psinl, psinr, yind, int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
