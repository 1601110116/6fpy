"""
plot the profiles (at omp) of the parameters that exists in the theoretical
dispersion relation
"""

from boutpy.boutdata import Case

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker, use
import os

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

#cases = ['/vol6/home/chaodong/BoutCases/2021.01/plot_resist/Rinnustar',
#         '/vol6/home/chaodong/BoutCases/2021.01/plot_resist/nl_Spresist',
#         '/vol6/home/chaodong/BoutCases/2021.01/plot_resist/nl_Zeff1']
#labels = [r'$\eta_{neo},Z_{eff}=2.8$',
#          r'$\eta_{Sptz},Z_{eff}=2.8$',
#          r'$\eta_{neo},Z_{eff}=1.0$']

cases = ['/vol6/home/chaodong/BoutCases/2021.01/n_scan/n50_260_128_0.89',
         '/vol6/home/chaodong/BoutCases/2021.01/n_scan/n50_260_128_wo_smooth',
         '/vol6/home/chaodong/BoutCases/2021.01/n_scan/n50_260_128_0.91']
labels = [r'0.89',
          r'0.90',
          r'0.91']

# cases = ['/vol6/home/chaodong/BoutCases/2021.01/plot_resist/nl_resist_scan/0.9x',
#          '/vol6/home/chaodong/BoutCases/2021.01/plot_resist/Rinnustar',
#          '/vol6/home/chaodong/BoutCases/2021.01/plot_resist/nl_resist_scan/1.1x']
# labels = [r'0.9$\times$',
#           r'1.0$\times$',
#           r'1.1$\times$']

psinl = 0.945
psinr = 1.0075
use('Agg')
# All cases should have the same following parameters

MU0 = 4.0e-7 * np.pi
fig, ax = plt.subplots(figsize=[6, 6], facecolor='w')
fig.subplots_adjust(hspace=0.3, wspace=0.5)
ax.set_xlabel(r'$\psi_n$')
ax.set_ylabel(r'$\eta_{\parallel}\ \mathrm{\left(\Omega\cdot m\right)}$')
ax.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax.grid(True)
case = cases[-1]
for icase in range(len(cases)):
    case = Case(cases[icase])
    xindl, psinl = case.gf.get_xind(psinl)
    xindr, psinr = case.gf.get_xind(psinr)
    # xindl = 0
    # xindr = 260
    yind_omp = case.gf.yind_omp
    psin = case.gf.get_psin(yind='omp')
    psin = psin[xindl: xindr+1]
    Va = case.di['Va']
    Lbar = case.di['Lbar']
    print('Va={},Lbar={}'.format(Va, Lbar))
    eta = case.collect('eta', xind=[xindl, xindr], yind=yind_omp, zind=15, nthreads=1).squeeze()
    # in Ohm-m
    eta *= Va * Lbar * MU0
    ax.plot(psin, eta, label=labels[icase])

xlim = ax.get_xlim()
ylim = ax.get_ylim()
# ax.text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left')
ax.legend()

# outname = os.path.join(case.eq_path, 'reist_profs_multips')
outname = os.path.join(case.eq_path, 'reist_profs_effects_notxt')
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
