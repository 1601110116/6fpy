"""
plot the profiles (at omp) of the parameters that exists in the theoretical
dispersion relation
"""

from boutpy.boutdata import Case

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker, use
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/test'
# case_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans/n50'
# case_path = '/vol6/home/chaodong/BoutCases/2021.09/nonlinear'
case_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans/n50_vdia'

use('Agg')
case = Case(case_path)

psin = case.gf.get_psin(yind='omp')
yind_omp = case.gf.yind_omp
# Ni0 = case.collect('N0', yind=yind_omp, nthreads=1).squeeze()
ee = 1.602e-19
Bxy = case.gf['Bxy'][:, yind_omp].squeeze()
Bbar = case.gf['bmag']
Va = case.di['Va']
Rxy = case.gf['Rxy'][:, yind_omp].squeeze()
Lbar = case.di['Lbar']
hthe = case.gf['hthe'][:, yind_omp].squeeze()
Bpxy = case.gf['Bpxy'][:, yind_omp].squeeze()
psixy = case.gf['psixy'][:, yind_omp].squeeze()
Tebar = case.di['Tebar']
density = case.di['density']
Nbar = case.di['Nbar']
Zi = float(case.inp['highbeta']['Zi'])

fig, ax = plt.subplots(2, 2, figsize=[18, 10], facecolor='w')
fig.subplots_adjust(hspace=0.3, wspace=0.65)
ax = ax.flatten()


Er0_dia_x = case.collect('Er0_dia_x', yind=yind_omp, nthreads=1).squeeze()
Er0_dia = Er0_dia_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)
Ve0_dia = - Er0_dia / Bxy
# ax[0].plot(psin[2: -2], Ve0_dia[2: -2], 'b-')
ax[0].plot(psin, Ve0_dia, 'b-')
# ax[2].plot(psin, Vde, 'b-')
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel(r'$v_{De}$ (m/s)')
ax[0].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[0].grid(True)
ax[0].set_title('Using Er0_dia_x')

Ne0 = case.collect('Ne0', yind=yind_omp, nthreads=1).squeeze()
Te0 = case.collect('Te0', yind=yind_omp, nthreads=1).squeeze()
Pe0 = Ne0 * Te0
dPe0dpsi = np.gradient(Pe0, psixy)
grad_psi = Rxy * Bpxy
grad_Pe0 = dPe0dpsi * grad_psi
# vde=grad_Pe0\times B/(Ne0B^2)>0. grad_Pe0<0 and r\times\bm{b}<0
# a ee should be multiplied and then devided, thus never exist
Vde0 = -Tebar * grad_Pe0 / (Ne0 * Bxy)
ax[1].plot(psin, Vde0, 'b-')
ax[1].set_xlabel(r'$\psi_n$')
ax[1].set_ylabel(r'$v_{De}$ (m/s)')
ax[1].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[1].grid(True)
ax[1].set_title('Using Pe0')

dNe0dpsi = np.gradient(Ne0, psixy)
grad_Ne0 = dNe0dpsi * grad_psi
Vde0_n = -Tebar * grad_Ne0 * Te0 / (Ne0 * Bxy)
ax[2].plot(psin, Vde0_n, 'b-')
ax[2].set_xlabel(r'$\psi_n$')
ax[2].set_ylabel(r'$v_{De,n}$ (m/s)')
ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[2].grid(True)

dTe0dpsi = np.gradient(Te0, psixy)
grad_Te0 = dTe0dpsi * grad_psi
Vde0_T = -Tebar * grad_Te0 / Bxy
ax[3].plot(psin, Vde0_T, 'b-')
ax[3].set_xlabel(r'$\psi_n$')
ax[3].set_ylabel(r'$v_{De,T}$ (m/s)')
ax[3].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[3].grid(True)

outname = os.path.join(case.eq_path, 'disp_profs_vdiamag.png')
fig.savefig(outname)
print('Figures have been written to ' + outname)
