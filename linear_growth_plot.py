import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 10,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

n_number = [25, 30, 35, 40, 45, 50, 60, 70]
with_gamma = [0.0032, 0.0078, 0.0115, 0.0144, 0.0166, 0.0183, 0.0207, 0.0221]
# without_gamma = [0.0008, 0.0012, 0.0011, 0.0003, -0.0007]

output_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans'


matplotlib.use('Agg')

fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')

ax.plot(n_number, with_gamma, 'b-o', label='w/ TF&eHall')
# ax.plot(n_number, without_gamma, 'r-o', label='w/o TF&eHall')
# lgd = ax.legend()
ax.grid(True)
ax.set_xlabel(r'$n$')
ax.set_ylabel(r'$\gamma\ \left(\mathrm{\tau_{A}^{-1}}\right)$')

outname = os.path.join(output_path, 'linear_growth_all')
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
