from boutpy.boutdata import Case

import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
import matplotlib

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
case_path = '/vol6/home/chaodong/BoutCases/2020.12/mz129'

var1_name = 'Ni'
var2_name = 'phi'
# psin = 0.975
psinl = 0.945
psinr = 1.0075
yind = 76
# tl = 1400
# tr = 1500
tl = -100
tr = -1
harmonic = 11

matplotlib.use('Agg')
case = Case(case_path)
xindl, psinl = case.gf.get_xind(psinl)
xindr, psinr = case.gf.get_xind(psinr)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
zperiod = int(case.inp['ZPERIOD'])

var1 = case.collect(var1_name, xind=[xindl, xindr], yind=yind, tind=[tl, tr], nthreads=1).squeeze()
var2 = case.collect(var2_name, xind=[xindl, xindr], yind=yind, tind=[tl, tr], nthreads=1).squeeze()

var1_k = np.fft.rfft(var1, axis=1)
var2_k = np.fft.rfft(var2, axis=1)
t = case.t_array[tl: tr + 1]


# var1_phase = np.angle(var1_k[:, harmonic, :]).squeeze()
# var2_phase = np.angle(var2_k[:, harmonic, :]).squeeze()
cross_phase = np.angle(var1_k[:, harmonic, :] / var2_k[:, harmonic, :]).squeeze()
# for it in range(0, tr - tl + 1):
#     if cross_phase[it] > np.pi:
#         cross_phase[it] = cross_phase[it] - 2 * np.pi
#     elif cross_phase[it] < -np.pi:
#         cross_phase[it] = cross_phase[it] + 2 * np.pi

t = case.t_array[tl: tr+1]
psin = case.gf.get_psin(yind='omp')
psin = psin[xindl: xindr+1]
fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
ct = ax.contourf(psin, t, cross_phase.transpose(), cmap=plt.get_cmap('jet'), levels=400)
fig.colorbar(ct, ax=ax, fraction=0.08, aspect=30)
ax.set_xlabel(r'$\psi_n$')
ax.set_ylabel(r't $\left(\tau_{A}\right)$')
xlim = ax.get_xlim()
ylim = ax.get_ylim()
ax.text(x=xlim[1], y=ylim[0], s='n={}'.format(zperiod * harmonic), va='bottom', ha='right')
ax.text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left', color='w')

var1_latex, _ = case.di.get_varname_latex(var1_name)
var2_latex, _ = case.di.get_varname_latex(var2_name)
ax.set_title(r'$S_{' + var1_latex +
              r'}-S_{' + var2_latex + '}$')

outname = os.path.join(
    case.pert_path, 'cross_phase_' + var1_name + '-' + var2_name +
                    '_psin{:.4f}-{:.4f}_iy{:03d}_t{:04d}-{:04d}'.format(
                        psinl, psinr, yind, int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
