from boutpy.boutdata import Case


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib

import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-8_hd4parapar1e-6'
time = 1500  # in Alfven time

matplotlib.use('Agg')
case = Case(case_path)
tind = case.get_tstep(time)

print('collecting Ni, Te and phi at tind={}'.format(tind))
Ni = case.collect('Ni', tind=tind, nthreads=1).squeeze()
Te = case.collect('Te', tind=tind, nthreads=1).squeeze()
phi = case.collect('phi', tind=tind, nthreads=1).squeeze()
Ni_rms = Ni.std(axis=-1)
Te_rms = Te.std(axis=-1)
phi_rms = phi.std(axis=-1)
_, Ninorm = case.di.get_normalization('Ni', value=True)
_, Tenorm = case.di.get_normalization('Te', value=True)
_, phinorm = case.di.get_normalization('phi', value=True)
Ni_rms *= Ninorm
Te_rms *= Tenorm
phi_rms *= phinorm

# find the (x, y) for maximum Ni_rms
x, y = np.unravel_index(np.argmax(Ni_rms), Ni_rms.shape)
Ni_t = case.collect('Ni', xind=x, yind=y, tind=[1, tind]).squeeze()
Ni_t_rms = Ni_t.std(axis=0)
Ni_t_rms *= Ninorm
log_Ni_t = np.log10(Ni_t_rms)

psin = case.gf.get_psin(yind='omp')
yind = list(range(case.gf['ny']))
yind_omp = case.gf.yind_omp
print('yind_omp = {}'.format(yind_omp))

fig, ax = plt.subplots(2, 2, figsize=[18, 8], facecolor='white')
fig.subplots_adjust(hspace=0.5, wspace=0.3)
ax = ax.flatten()
fig.suptitle(r't={} $\tau_A$'.format(int(case.get_time(tind))))

ct0 = ax[0].contourf(psin, yind, Ni_rms.transpose(), cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct0, ax=ax[0], fraction=0.08, aspect=30)
ax[0].plot(psin[x], y, 'wx', ms=15, lw=2, label=
            'x={}, y={}\n'.format(x, y) +
            '$\psi_{{n}}={:.4f}$'.format(psin[x]))
lgd0 = ax[0].legend(loc='lower left')
for text in lgd0.get_texts():
    text.set_color('w')
ax[0].axvline(x=1, color='w', linewidth=1)
ax[0].axhline(y=yind_omp, color='w', linewidth=1)
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel('y index')
ax[0].set_title(r'$n_{rms}\ \left(10^{{20}}\ \mathrm{m^{{-3}}}\right)$')

t = case.t_array[1: tind+1]
Tbar = case.di['Tbar']
ax[1].plot(t, log_Ni_t, 'b-')
ax[1].legend(title=r'$\tau_A={:.2e}$ s'.format(Tbar), loc='lower right')
ax[1].set_xlabel(r't $(\tau_A)$')
ax[1].set_ylabel(r'$\lg\left[n_{rms}\ \left(10^{{20}}\ \mathrm{m^{{-3}}}\right)\right]$')
ax[1].grid(True)

ct2 = ax[2].contourf(psin, yind, Te_rms.transpose(), cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct2, ax=ax[2], fraction=0.08, aspect=30)
ax[2].axvline(x=1, color='w', linewidth=1)
ax[2].axhline(y=yind_omp, color='w', linewidth=1)
ax[2].set_xlabel(r'$\psi_n$')
ax[2].set_ylabel('y index')
ax[2].set_title(r'$T_{e,rms}\ \left(\mathrm{eV}\right)$')

ct3 = ax[3].contourf(psin, yind, phi_rms.transpose(), cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct3, ax=ax[3], fraction=0.08, aspect=30)
ax[3].axvline(x=1, color='w', linewidth=1)
ax[3].axhline(y=yind_omp, color='w', linewidth=1)
ax[3].set_xlabel(r'$\psi_n$')
ax[3].set_ylabel('y index')
ax[3].set_title(r'$\phi_{rms}\ \left(\mathrm{V}\right)$')

outname = os.path.join(case.pert_path, 'mode_structures_t{:04d}'.format(
    int(case.get_time(tind))))
print('Figures have been written to ' + outname)
fig.savefig(outname)

