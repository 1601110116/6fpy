from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os
import matplotlib

rcParams.update(
    {"font.size": 28,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 10,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
# average over this time range to get mode rms radial distribution
# tl = 1000  # in Alfven time
# tr = 1500
tl = 750  # in Alfven time
tr = 2000
yind = 76
psinl = 0.93
psinr = 1.02

matplotlib.use('Agg')
case = Case(case_path)

tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)

Ni_x = case.collect('Ni', yind=yind, tind=[tl, tr], nthreads=1).squeeze()
Nirms = Ni_x.std(axis=1)
_, Ninorm = case.di.get_normalization('Ni', value=True)
Nirms *= Ninorm
Nirms_tavg = Nirms.mean(axis=-1).squeeze()

Er0_x = case.collect('Er0_x', yind=yind, nthreads=1).squeeze()
Rxy = case.gf['Rxy'][:, yind]
Zxy = case.gf['Zxy'][:, yind]
Bpxy = case.gf['Bpxy'][:, yind]
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Va = case.di['Va']
Er0 = Er0_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)

fig, ax = plt.subplots(figsize=[8, 6], facecolor='w')
ax.grid(True)
ax.tick_params(axis='y', labelcolor='b')
ax.plot(psin[xindl: xindr+1], Nirms_tavg[xindl: xindr+1], 'b-')
ax.set_xlabel(r'$\psi_n$')
ax.set_ylabel(r'$n_{i,rms}\ \left(\mathrm{10^{{20}}\ m^{{-3}}}\right)$')
ax.yaxis.label.set_color('b')
ylim = ax.get_ylim()
# ax.text(x=psin[xindl], y=ylim[1], s='(a)', va='top', ha='left')
xind_peak = np.argmax(Nirms_tavg)
xind_peak, _ = case.gf.get_xind(0.988)
ax.axvline(psin[xind_peak], color='g')

ax_Er = ax.twinx()
ax_Er.tick_params(axis='y', labelcolor='r')
ax_Er.plot(psin[xindl: xindr+1], Er0[xindl: xindr+1] * 1e-3, 'r-')
ax_Er.set_ylabel(r'$E_{r0}\ \left(\mathrm{kV/m}\right)$')
ax_Er.yaxis.label.set_color('r')

outname1 = os.path.join(case.pert_path,
                       'Nirms_Er_y{:03d}'.format(yind))
fig.savefig(outname1 + '.png')
fig.savefig(outname1 + '.eps')
print('Figures have been written to ' + outname1)

fig2, ax2 = plt.subplots(figsize=[8, 6], facecolor='w')
Ni_t = case.collect('Ni', xind=xind_peak, yind=yind, tind=[1, tr], nthreads=1).squeeze()
Ni_t_rms = Ni_t.std(axis=0)
Ni_t_rms *= Ninorm
log_Ni_t = np.log10(Ni_t_rms)
time = case.t_array[1: tr+1]
ax2.plot(time, log_Ni_t, 'b-')
ax2.set_xlabel(r'$t\ \left(\tau_A\right)$')
ax2.set_ylabel(r'$\lg\left[n_{i,rms}\ \left(10^{{20}}\ \mathrm{m^{{-3}}}\right)\right]$')
ax2.grid(True)
ax2.legend(title=r'$\psi_n$={:.3f}'.format(psin[xind_peak]), loc='lower right')
ylim = ax2.get_ylim()
# ax2.text(x=psin[xindl], y=ylim[1], s='(b)', va='top', ha='left')
outname2 = os.path.join(case.pert_path,
                       'Nirms_t{:04d}-{:04d}y{:03d}'.format(int(case.get_time(tl)),
                                                                   int(case.get_time(tr)), yind))
fig2.savefig(outname2 + '.png')
fig2.savefig(outname2 + '.eps')
print('Figures have been written to ' + outname2)
