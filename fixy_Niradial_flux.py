from boutpy.boutdata import Case
from boutpy.boutdata import collect

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-8_hd4parapar1e-6'
case_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans/n50'
tl = -11  # in Alfven time
tr = -6
dt = 5
psinl = 0.945
psinr = 1.01
yind = 76

case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
timestep = case.di.get_timestep()
dt = floor(float(dt) / timestep)
print('tl_ind={}, tr_ind={}, dt={}'.format(tl, tr, dt))
tind = np.asarray(list(range(tl, tr+1, dt)))
nt = len(tind)
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)
print('xindl={}, xindr={}'.format(xindl, xindr))

Va = case.di['Va']
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Nbar = case.di['Nbar']
density = case.di['density']
Rx = case.gf['Rxy'][:, yind].squeeze()
Bpx = case.gf['Bpxy'][:, yind].squeeze()
psix = case.gf['psixy'][:, yind].squeeze()
nx = case.di.nx
ny = case.di.ny
nz = case.di.nz

Ni = np.zeros([nx, nz, nt])
Vexbx = Ni.copy()
print('collecting Ni and Vexbx : ')
for it in range(nt):
    print('\ttind={}'.format(tind[it]))
    Ni[:, :, it] = case.collect('Ni', tind=tind[it], yind=yind, nthreads=1).squeeze()
    Vexbx[:, :, it] = case.collect('Vexbx', tind=tind[it], yind=yind, nthreads=1).squeeze()
print('collecting N0 :')
Ni0 = case.collect('N0', yind=yind, nthreads=1).squeeze()

# the radial component is determined solely by the contravariant x component
Vexbr = Vexbx / np.reshape((Rx/Lbar) * (Bpx/Bbar), (nx, 1, 1))
# Vexbr = np.ones_like(Ni)

# Ni gradient, calculated with SI units
drdpsi = 1 / (Rx * Bpx)
dpsi = np.gradient(psix)
dr = drdpsi * dpsi
rx = integrate.cumtrapz(dr, axis=0, initial=0)
dNidr = np.gradient(Ni0 * (Nbar * density / 1e20), rx)
dNidr = dNidr[xindl: xindr+1]

# We only include electrostatic Ni flux for now, since we assume Vi=0
# Ni0 is neglected since we do not have equilibrium radial EXB flow, <Ni0*Vexbr>=0
Nirflux_es = Ni * Vexbr * (Nbar * density/1e20 * Va)
Nirflux_es_surface_avg = np.mean(Nirflux_es, axis=1)
Nirflux_es_surface_avg = Nirflux_es_surface_avg[xindl: xindr+1, :]

t = case.t_array[tind]
fig, ax = plt.subplots(2, 2, figsize=[12, 12], facecolor='white')
fig.subplots_adjust(hspace=0.3, wspace=0.6)
fig.suptitle('yind = {}'.format(yind))
ax = ax.flatten()

# ax[0].grid(True)
ct0 = ax[0].contourf(t, psin[xindl: xindr+1], Nirflux_es_surface_avg, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct0, ax=ax[0], fraction=0.08, aspect=30)
ax[0].set_xlabel(r'$t\ (\tau_A)$')
ax[0].set_ylabel(r'$\psi_n$')
ax[0].set_title(r'$\left<\Gamma_{n,r}\right>_{surf}\ (10^{20}m^{-3}m/s)$')

Di = -Nirflux_es_surface_avg / np.reshape(dNidr, (xindr - xindl + 1, 1))
# ax[1].grid(True)
ct1 = ax[1].contourf(t, psin[xindl: xindr+1], Di, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct1, ax=ax[1], fraction=0.08, aspect=30)
ax[1].set_xlabel(r'$t\ (\tau_A)$')
ax[1].set_ylabel(r'$\psi_N$')
ax[1].set_title(r'$D_i\ (m^2/s)$')

Nirflux_es_tavg = np.mean(Nirflux_es_surface_avg, axis=1)
ax[2].plot(psin[xindl: xindr+1], Nirflux_es_tavg, 'r-')
ax[2].set_xlabel(r'$\psi_n$')
ax[2].set_ylabel(r'$\left<\Gamma_{n,r}\right>_t\ (10^{20}m^{-3}m/s)$')
ax[2].grid(True)


Di_tavg = np.mean(Di, axis=1)
ax[3].plot(psin[xindl: xindr+1], Di_tavg, 'b-')
ax[3].set_xlabel(r'$\psi_n$')
ax[3].set_ylabel(r'$\left<D_i\right>_t\ (m^2/s)$')
ax[3].grid(True)


outname = os.path.join(case.pert_path, 'Niflux_t{:04d}-{:04d}_y{:03d}'.format(
    int(case.get_time(tl)), int(case.get_time(tr)), yind))
fig.savefig(outname)
print('Figures have been written to ' + outname)
