from boutpy.boutdata import Case

import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
import matplotlib

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2021.09/linear_scans/n50'
# case_path = '/vol6/home/chaodong/BoutCases/2021.06/tf_sptz_resist'

var1_name = 'Ni'
var2_name = 'phi'
# psin = 0.975
psinl = 0.945
psinr = 1.0075
yind = 76
tl = 300
tr = 300
harmonic = 1
phase_min = -2  # cross_phase is refined in the range (phase_min, phase_min+2*pi)

matplotlib.use('Agg')
case = Case(case_path)
xindl, psinl = case.gf.get_xind(psinl)
xindr, psinr = case.gf.get_xind(psinr)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
zperiod = int(case.inp['ZPERIOD'])

# specify the axis of squeeze() in case only one time step is collected
var1 = case.collect(var1_name, xind=[xindl, xindr], yind=yind, tind=[tl, tr], nthreads=1).squeeze(axis=1)
var2 = case.collect(var2_name, xind=[xindl, xindr], yind=yind, tind=[tl, tr], nthreads=1).squeeze(axis=1)

var1_k = np.fft.rfft(var1, axis=-2)
var2_k = np.fft.rfft(var2, axis=-2)
t = case.t_array[tl: tr + 1]


# this is \vartheta_{var2} - \vartheta_{var1} in the form e^{-in\zeta+ivartheta}
cross_phase = np.angle(var1_k[:, harmonic, :] / var2_k[:, harmonic, :])
cross_phase[cross_phase<phase_min] += 2*np.pi
# time average
cross_phase = np.mean(cross_phase, axis=-1).squeeze()

# for it in range(0, tr - tl + 1):
#     if cross_phase[it] > np.pi:
#         cross_phase[it] = cross_phase[it] - 2 * np.pi
#     elif cross_phase[it] < -np.pi:
#         cross_phase[it] = cross_phase[it] + 2 * np.pi

t = case.t_array[tl: tr+1]
psin = case.gf.get_psin(yind='omp')
psin = psin[xindl: xindr+1]
fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
ax.plot(psin, cross_phase, 'r-')
ax.grid(True)
ax.set_xlabel(r'$\psi_n$')
var1_latex, var1_unit = case.di.get_varname_latex(var1_name)
var2_latex, var2_unit = case.di.get_varname_latex(var2_name)
ax.set_ylabel(r'$\vartheta_{' + var2_latex + r'}-\vartheta_{' + var1_latex + '}$'
              + ', n={}'.format(zperiod*harmonic))
ax.tick_params(axis='y', labelcolor='r')
ax.yaxis.label.set_color('r')
xlim = ax.get_xlim()
ylim = ax.get_ylim()
ax.text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left')

var2_rms = var2.std(axis=1)
_, var2_norm = case.di.get_normalization(var2_name, value=True)
var2_rms *= var2_norm
var2_rms_tavg = var2_rms.mean(axis=-1).squeeze()

ax_var2 = ax.twinx()
ax_var2.tick_params(axis='y', labelcolor='b')
ax_var2.plot(psin, var2_rms_tavg, 'b-')
ax_var2.set_ylabel(r'$\phi_{rms}$ (V)')
ax_var2.yaxis.label.set_color('b')


outname = os.path.join(
    case.pert_path, 'cross_phase_h{:02d}_'.format(harmonic) + var1_name + '-' + var2_name +
                    '_psin{:.4f}-{:.4f}_y{:03d}_t{:04d}-{:04d}'.format(
                        psinl, psinr, yind, int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
