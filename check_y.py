from boutpy.boutdata import boutgrid
from boutpy.boutdata import collect
import matplotlib.pyplot as plt
import numpy as np
from boutpy.boutdata import Case
from boutpy.boututils import DmpInfo
import os

case_path = '../2020.06/3rdexpErF_n30'

case = Case(case_path=case_path)
nt = case.di.nt - 1
tind = nt-1
var_name = 'Te'

# var = collect(var_name, xind=(200,400), yind=(150,180), tind=tind, nthreads=1, zind=5).squeeze()
var = collect(var_name, path=case.data_path, tind=tind, nthreads=1, zind=5).squeeze()
fig, ax = plt.subplots()
ax.contourf(var.squeeze().T, levels=80, cmap=plt.cm.bwr)
ax.set_xlabel('ix')
ax.set_ylabel('iy')
fig.savefig('{}t{:03d}check_y.png'.format(var_name, tind), dpi=500)
plt.show()

case.check_bxcv()
