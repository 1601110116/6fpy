"""
Compare Spitzer-Harm parallel conductivity and (\alpha * free-streaming) conductivity.
Warning: This script is designed only for output_kappa_par=false
"""

from boutpy.boutdata import Case
from boutpy.boutdata import collect

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
from matplotlib import ticker
import os


rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2021.01/n_scan/n50'
case_path = '/vol6/home/chaodong/BoutCases/2021.01/n_scan/n60_constq'

zind = 1
yind = 76
psinl = 0.99
psinr = 1.04

case = Case(case_path)
if not case.inp['highbeta']['output_kappa_par'] == 'false':
    raise ValueError('This script is designed only for output_kappa_par=false')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)

print('collecting kappa_par_i_sp, kappa_par_i_fl and kappa_par_i')
# Spitzer-Harm
kappa_par_i_sp = collect('kappa_par_i_sp', path=case.data_path, yind=yind, zind=zind, nthreads=1).squeeze()
# \alpha * free-streaming
kappa_par_i_fl = collect('kappa_par_i_fl', path=case.data_path, yind=yind, zind=zind, nthreads=1).squeeze()
kappa_par_i    = collect('kappa_par_i',    path=case.data_path, yind=yind, zind=zind, nthreads=1).squeeze()

print('collecting kappa_par_e_sp, kappa_par_e_fl and kappa_par_e')
kappa_par_e_sp = collect('kappa_par_e_sp', path=case.data_path, yind=yind, zind=zind, nthreads=1).squeeze()
kappa_par_e_fl = collect('kappa_par_e_fl', path=case.data_path, yind=yind, zind=zind, nthreads=1).squeeze()
kappa_par_e    = collect('kappa_par_e',    path=case.data_path, yind=yind, zind=zind, nthreads=1).squeeze()

print('normalizing ...')
Lbar = case.di['Lbar']
Va = case.di['Va']
Nbar = case.di['Nbar']
density = case.di['density']
Nbar = Nbar * density / 1e20
norm = 1.5 * Lbar * Va * Nbar  # in m^2/s * 1e20m^-3
kappa_par_i_sp = norm * kappa_par_i_sp[xindl: xindr+1]
kappa_par_i_fl = norm * kappa_par_i_fl[xindl: xindr+1]
kappa_par_i    = norm * kappa_par_i[xindl: xindr+1]
kappa_par_e_sp = norm * kappa_par_e_sp[xindl: xindr+1]
kappa_par_e_fl = norm * kappa_par_e_fl[xindl: xindr+1]
kappa_par_e    = norm * kappa_par_e[xindl: xindr+1]

print('plotting ...')
psin = case.gf.get_psin(yind='omp')
psin = psin[xindl: xindr+1]
fig, ax = plt.subplots(1, 2, figsize=[12, 6], facecolor='white')
fig.subplots_adjust(wspace=0.5, hspace=0.3)
ax = ax.flatten()
ax[0].grid(True)
ax[1].grid(True)
ax[0].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))
ax[1].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.1e'))

# ax[0].plot(psin, kappa_par_i_sp, 'b-', label=r'$\kappa_{\parallel i}$')
ax[0].plot(psin, kappa_par_i_fl, 'r-', label=r'$\kappa_{fs, i}^{\prime}$')
ax[0].plot(psin, kappa_par_i,    'k-', label=r'$\kappa_{eff, i}$')
ax[0].legend()
ax[0].set_title('ion')
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel(r'$\kappa_{\parallel} \ (10^{20}m^{-3}m^2/s)$')

# ax[1].plot(psin, kappa_par_e_sp, 'b-', label=r'$\kappa_{\parallel e}$')
ax[1].plot(psin, kappa_par_e_fl, 'r-', label=r'$\kappa_{fs, e}^{\prime}$')
ax[1].plot(psin, kappa_par_e,    'k-', label=r'$\kappa_{eff, e}$')
ax[1].legend()
ax[1].set_title('electron')
ax[1].set_xlabel(r'$\psi_n$')
ax[1].set_ylabel(r'$\kappa_{\parallel} \ (10^{20}m^{-3}m^2/s)$')

outname = os.path.join(case.eq_path, 'kappa_par_y{:03d}x{:03d}-{:03d}.png'.format(
    yind, xindl, xindr))
fig.savefig(outname)
print('Figures have been written to ' + outname)