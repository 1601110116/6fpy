from boutpy.boutdata import Case

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 10,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
case_path = '/vol6/home/chaodong/BoutCases/2021.09/nonlinear'
# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar_Tscan/T1.2x'
# We use the time range of [tl, tr]
# tl = 1000  # in Alfven time
# tr = 1500
tl = 750  # in Alfven time
tr = -1
fmax = 500
# psin = 0.99
psin = 0.988
yind = 76
var_name = 'Ni'

case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
xind, _ = case.gf.get_xind(psin)

var_t = case.collect(var_name, xind=xind, yind=yind, tind=[tl, tr], nthreads=1).squeeze()
_, norm = case.di.get_normalization(var_name, value=True)
var_t *= norm
var_f = np.fft.rfft(var_t, axis=-1)
freq = np.fft.rfftfreq(var_t.shape[-1],
                       float(case.inp['TIMESTEP']) * case.di['Tbar'])
freq *= 1e-3  # convert from Hz to kHz
spct = (1/np.sqrt(2)) * np.abs(var_f) / (tr - tl + 1) * 2
spct = spct.mean(axis=0)
ifmax = np.abs(fmax - freq).argmin()
freq = freq[:ifmax + 1]
spct = spct[:ifmax + 1]

fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
ax.grid(True)
ax.plot(freq, spct, 'b-o')
ax.set_xlabel('$f$ (kHz)')
ax.set_ylabel(r'$n_{i1}\ \left(\mathrm{10^{20}\ m^{-3}}\right)$')
ylim = ax.get_ylim()
# ax.text(x=0, y=ylim[1], s='(a)', va='top', ha='left')

outname = os.path.join(
    case.pert_path, 'frq_sqectrum_t{:04d}-{:04d}ix{:03d}iy{:03d}'.format(
        int(case.get_time(tl)), int(case.get_time(tr)), xind, yind))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
