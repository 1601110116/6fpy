%% To be done

clear all;
%close all;

set(groot,'DefaultLineLinewidth',2);
set(groot,'DefaultAxesFontSize',20);
set(groot,'DefaultTextFontSize',20);

% part 0: control parameter
% xi=130;

% part 1: load data
fid = netcdf.open('./d3d_163518_3130_nx260ny64_psi080110_1peak_v1.nc', 'nc_nowrite');
vid = netcdf.inqVarID( fid, 'psi_axis' );   psi_axis   = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'psi_bndry' );  psi_bndry  = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'psixy' );      psixy      = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'ShiftAngle' ); shiftangle = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'Rxy' );        rxy        = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'Zxy' );        zxy        = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'E_r' );        er         = netcdf.getVar( fid, vid );
vid = netcdf.inqVarID( fid, 'zShift' );     zshift     = netcdf.getVar( fid, vid );

clear vid;
netcdf.close(fid);

% figure(900)
% f0=er;
% nx=260;ny=64;
% isepx1=174;isepx2=179;iyp=4;
% % first adding two lines in radial direction
% fn=zeros(262,66);
% fn_tmp(1:isepx1,:)=f0(1:isepx1,:);
% fn_tmp(ispex1+1,:)=0.5*(f0(isepx,:)+f0(isepx+1,:));
% fn_tmp(isepx1+2:isepx2+1)=f0(isepx1+1:isepx2,:);
% fn_tmp(isepx2+3:nx+2)=f0(isepx2+1:end,:);
% isepx1=isepx1+1; isepx2=ispex2+2; % real (close to) speratrix location now
% % now, tow lines in poloidal direction
% yin=[1:iyp iyp+2:iyp+25 iyp+27 iyp]
% fn(1:iyp)
% 
% ix1=[1:174]; iy1=[5:28 37:60 5];
% pcolor(rxy(iy1,ix1),zxy(iy1,ix1),er(iy1,ix1));

psi=(psi_axis-psixy(16,:))/(psi_axis-psi_bndry);
q_global =-shiftangle/(2.*pi);

raw_data = restore_idl('./t21_ni_y47_48.sav');
%raw_data = restore_idl('./t23_ni_y47_48.sav');
%raw_data = restore_idl('./t21_psi_y47_48_t2000.sav');

%%%%control parameters
select_type = 3; %chose the select type, 1-manual, 2-auto, 3-fixed
radial_scan = 1; %calcute the raial distribution

test_run = 1; %calcute the raial distribution

% [nx,ny,nz,nt] = size(p1);
% % 
% tuns = tun;
% time = tuns*[1:nt];
% % 
%  nx1 = 134;              %%%the location of the data
%  nx2 = 134;
%  ny1 = 38;
% ny2 = 39;
% nz0 = 1;
% n_rms(nx1,ny1,1) = 1;
% n_rms(nx2,ny2,1) = 1;
% %minor and major radius of the tokamak;
R = 1.67;
a=0.67;  %for the DIII-D
% % 
% % R = 3.;    % for Circular
% % a = 1.2; 
% 
% R = 0.68;   % for Cmod
% a = 0.22;
% 
% % R = 6.2;   % for ITER
% % a = 2.0;
% % 
% q_local = 3.68;
% % %the distance in the y direction
% d_y = (ny1-ny2)/(64-8)*2*pi*(0.9*a+nx1/260*(1.04-0.9)*a);
%if radial_scan == 1
    
    %for xi=1:1:260
    %for xi=139:139
    for xi=130:130
        xi

% % % %get the distance in poloidal direction convert into centimeter
d_y = sqrt((rxy(48,xi)-rxy(49,xi))^2+(zxy(48,xi)-zxy(49,xi))^2+...
    (2*pi*(R+a)/5/64)^2);
%d_y = sqrt((rxy(16,xi)-rxy(17,xi))^2+(zxy(16,xi)-zxy(17,xi))^2);
q_local = q_global(xi);
d_sep = -d_y*100/sqrt((R*q_local/a)^2+1);

ddd(xi)=d_sep;
% % %d_sep = 9.4127063e-03;
%    end
    
%d_sep = -0.3/1.2601246;
%d_sep = 0.17624105;
%d_sep = 0.050545471530641; % in m
%nt=1116;
%nt=4414; %size(VarName1);
%nt=1041;
%nt=2775;
%tuns=1.38361e-7;

%nt=4415;
%tuns=1.237449e-07; % for wide-pedestal QH-mode

nt=5649;
tuns=1.235596e-7;

%if test_run == 1
    
    var1 = squeeze(raw_data.NI(:,1,:,:));
    var2 = squeeze(raw_data.NI(:,2,:,:));
    
    for zi=1:64
        zi;
    time = tuns*[1:nt];    
 %   vf1 = squeeze(var1(xi,zi,:));
 %   vf2 = squeeze(var2(xi,zi,:));

    dz=2*pi/64/5;
    zangle=dz*(zi-1);
    zind1 = (zangle - zshift(48, xi)) / dz + 1;
    zind2 = (zangle - zshift(49, xi)) / dz + 1;
    for ttt=1:nt
        vf1(ttt) = zinterp( squeeze(var1(xi, :, ttt)), zind1, 64);
        vf2(ttt) = zinterp( squeeze(var2(xi, :, ttt)), zind2, 64);
    end
    

    %vf1 = squeeze(p1(nx1,ny1,nz0,:))./squeeze(n_rms(nx1,ny2,:));
    %vf2 = squeeze(p1(nx2,ny2,nz0,:))./squeeze(n_rms(nx2,ny2,:));
%    vf1 = squeeze(VarName1(:));%./squeeze(v1_rms(:));
%    vf2 = squeeze(VarName2(:));%./squeeze(v2_rms(:));

%    vf1 = squeeze(data.NI1(134,1,1,:));
%    vf2 = squeeze(data.NI1(134,2,1,:));
%    vf1 = squeeze(data.NI1WOC(134,1,1,:))./sqrt(squeeze(mean(data.NI1WOC(134,1,:,:).^2,3)));
%    vf2 = squeeze(data.NI1WOC(134,2,1,:))./sqrt(squeeze(mean(data.NI1WOC(134,2,:,:).^2,3)));

%    vf1 = squeeze(data.NI1WOC(134,1,1,:));
%    vf2 = squeeze(data.NI1WOC(134,2,1,:));
%    vf1 = squeeze(data.NI1WOC(138,1,1,:))./sqrt(squeeze(mean(data.NI1WOC(138,1,:,:).^2,3)));
%    vf2 = squeeze(data.NI1WOC(138,2,1,:))./sqrt(squeeze(mean(data.NI1WOC(138,2,:,:).^2,3)));

%    vf1 = squeeze(data.NI2(134,1,1,:));
%    vf2 = squeeze(data.NI2(134,2,1,:));
%    vf1 = squeeze(data.NI2(134,1,1,:))./sqrt(squeeze(mean(data.NI2(134,1,:,:).^2,3)));
%    vf2 = squeeze(data.NI2(134,2,1,:))./sqrt(squeeze(mean(data.NI2(134,2,:,:).^2,3)));
    
%    vf1 = squeeze(data.NI3(134,1,1,:));
%    vf2 = squeeze(data.NI3(134,2,1,:));
%    vf1 = squeeze(data.NI3(134,1,1,:))./sqrt(squeeze(mean(data.NI3(134,1,:,:).^2,3)));
%    vf2 = squeeze(data.NI3(134,2,1,:))./sqrt(squeeze(mean(data.NI3(134,2,:,:).^2,3)));
    % % 
%     plot(time*1000,vf1,'k'),hold on
%     plot(time*1000,vf2,'r')


%    xlabel('time(ms)')
    % plot(Vf1),hold on
    % plot(Vf2,'r')
    % xlabel('time/10t_A')
    % ylabel('')

    if select_type ==1
    % % % select the range artificially
        [ta,ay]=ginput(1);
        [tb,by]=ginput(1);

        [r1,Ia]=min(abs(time*1000-ta));
        [r2y,Ib]=min(abs(time*1000-tb));
    elseif select_type ==2
    % %auto select
        Ib = length(time);
        Ia = length(time)-800;
    elseif select_type ==3
        Ib = 5000;%length(time);
        Ia = 2501;
    end

    Vf1 = vf1(Ia:Ib);
    Vf2 = vf2(Ia:Ib);
    time = time(Ia:Ib);
%     figure(1)
%     plot(time*1000,Vf1,'ko')
%     plot(time*1000,Vf2,'rx')
    % 


    % % Skew = skewness(squeeze(p1(:,38,:,Ia:Ib)),1,3);
    % % Kur = kurtosis(squeeze(p1(:,38,:,Ia:Ib)),1,3);
    % % Skew_mean = mean(Skew(:,1:end-1),2);
    % % Kur_mean = mean(Kur(:,1:end-1),2);
    % % figure(100)
    % % subplot(2,1,1)
    % % plot(psi,Skew_mean,'Linewidth',3)
    % % xlabel('\psi_N');ylabel('Skewness')
    % % hold on
    % % plot([psi(1) psi(end)],[0 0],'--','Linewidth',3,'color','k')
    % % xlim([0.95 1.04])
    % % subplot(2,1,2)
    % % plot(psi,Kur_mean,'Linewidth',3)
    % % xlabel('\psi_N');ylabel('Kurtosis')
    % % xlim([0.95 1.04])
    % 
    % % nbins = -0.6:0.01:0.6;
    % % powerdf = zeros(nx,length(nbins));
    % % % gaussfit = zeros(nx,length(nbins));
    % % for i=1:nx
    % %     powerdf(i,:) = histfit(squeeze(p1(i,38,1,Ia:Ib)),nbins);
    % % %     gaussfit(i,:) = fit(nbins,powerdf(i,:),'gauss2');
    % % end

    factor =1;
    Vf1_try = zeros(1,factor*length(Vf1));
    Vf2_try = zeros(1,factor*length(Vf2));
    time_try = zeros(1,factor*length(time));
    for i =1:length(Vf1_try)
        index = floor(i/length(Vf1));
        if index==0
            Vf1_try(i) = Vf1(i);
            Vf2_try(i) = Vf2(i);
        elseif index > 0
            Vf1_try(i) = Vf1(i-index*length(Vf1)+1);
            Vf2_try(i) = Vf2(i-index*length(Vf2)+1);
        end
        time_try(i) = time(1)+i*tuns;
    end


    Vf1_try = Vf1_try - mean(Vf1_try);
    Vf2_try = Vf2_try - mean(Vf2_try);


%     figure(50)
%     plot(time_try,Vf1_try);hold on
%     plot(time_try,Vf2_try)
%     legend('first','second')



    % close all
    % fs = 1e6;
    % ts = 1/fs;
    % d = -0.7;
    % t0 = 0;
    % t1 = 0.005;
    % time = t0:ts:t1;
    % f0 = 100000;
    % f00 = 30000;
    % f1 = zeros(1,length(time));
    % f2 = zeros(1,length(time));
    % f10 = zeros(1,length(time));
    % f20 = zeros(1,length(time));
    % for i =1:length(time)
    %     f1(i) = f0+1.0*rand(1);
    %     f2(i) = f0+1.0*rand(1);
    %     f10(i) = f00+.5*rand(1);
    %     f20(i) = f00+.5*rand(1);
    % end
    % Vf1 = sin(2*pi*time.*f1)+sin(2*pi*time.*f10);
    % Vf2 = sin(2*pi*time.*f2+pi/3)+sin(2*pi*time.*f20-pi/16);
    % figure(5)
    % ha(1) = subplot(2,1,1);
    % plot(time,Vf1,'r-');legend('vf1')
    % ha(2) = subplot(2,1,2);
    % plot(time,Vf2,'k-');legend('vf2')
    % linkaxes(ha,'x')
    % % SKF(time,d,Vf1,Vf2)
    clear k f s p q SKF;
    % [k,f,s,lc,SKF] = SKF(time,d,Vf1,Vf2);
    [k,f,s,p,q,lc,vp,SKF,rkf] = SKF(time_try,d_sep,Vf1_try,Vf2_try);
    % [k,f,s,mf12,mk12,lc,SKF] = SKF_try(time_try,d_sep,Vf1_try,Vf2_try);
    
    rkft(:,:,zi)=rkf;
    end
rkfm=mean(rkft,3);
%end

rk(xi,:)=k; rf(xi,:)=f;
rkfxm(xi,:,:) = rkfm;

    end
    
% if radial_scan ==1
%     close all
%     nx = 260;
%     ny1 = 38;
%     ny2 = 39;
%     nz0 = 1;
%     for ix =133:1:134
%         ix
%         pause(1);
% %    vf1 = squeeze(p1(ix,ny1,nz0,:))./squeeze(n_rms(ix,ny1,:));
% %    vf2 = squeeze(p1(ix,ny2,nz0,:))./squeeze(n_rms(ix,ny2,:));
% 
% %    vf1 = squeeze(data.NI1WOC(ix,1,1,:));
% %    vf2 = squeeze(data.NI1WOC(ix,2,1,:));
%     vf1 = squeeze(data.NI1WOC(ix,1,1,:))./sqrt(squeeze(mean(data.NI1WOC(ix,1,:,:).^2,3)));
%     vf2 = squeeze(data.NI1WOC(ix,2,1,:))./sqrt(squeeze(mean(data.NI1WOC(ix,2,:,:).^2,3)));
% 
% %     figure
% %     plot(vf1);
% %     hold on
% %     plot(vf2);
% %     hold off
%     
%     Vf1 = vf1(end-1400:end);
%     Vf2 = vf2(end-1400:end);
%     time = time(end-1200:end);
%     %%%% copy the data
%     factor =2;
%     Vf1_try = zeros(1,factor*length(Vf1));
%     Vf2_try = zeros(1,factor*length(Vf2));
%     time_try = zeros(1,factor*length(time));
%     for i =1:length(Vf1_try)
%         index = floor(i/length(Vf1));
%         if index==0
%             Vf1_try(i) = Vf1(i);
%             Vf2_try(i) = Vf2(i);
%         elseif index > 0
%             Vf1_try(i) = Vf1(i-index*length(Vf1)+1);
%             Vf2_try(i) = Vf2(i-index*length(Vf2)+1);
%         end
%         time_try(i) = time(1)+i*tuns;
%     end
%     Vf1_try = Vf1_try - mean(Vf1_try);
%     Vf2_try = Vf2_try - mean(Vf2_try);
%     
%     clear k f s SKF;
%     [k,f,s,p,q,lc,SKF] = SKF2(time_try,d_sep,Vf1_try,Vf2_try);
% %     k_scan(ix,:) = k;
% %     f_scan(ix,:) = f;
%     power(ix,:,:) = p;
%     phase(ix,:,:) = q;
% 
%     
%     end
% end

figure(1)
pcolor(k,f,rkf);shading flat;
%pcolor(k,f,log10(rkf));shading flat;
load './kfspectrum/colormap.mat';colormap(MAP);
%xlim([-.3,.3]);ylim([-200,200])
%caxis([0,0.01])

%save('qh_t21_n_15_16_rkfxm.mat','rk','rf','rkfxm');

% part X:

load('qh_t21_n_47_48_rkfxm.mat')
load './kfspectrum/colormap.mat';
colormap(MAP);
dframes = 4; fform = strcat('%0',num2str(dframes),'d');
figure(111)
for k=1:2:260
    %figure(k+1)
    pcolor(rk(k,:),rf(k,:),squeeze(rkfxm(k,:,:)));shading flat;colorbar
    xlim([-1,1]);ylim([-300,300]);colormap(MAP);
    hold on
    plot([0 0],[-500 500],'--','color',[0.5 0.5 0.5])
    plot([-2 2],[0 0],'--','color',[0.5 0.5 0.5])
    ylabel('Freq. (kHz)','fontsize',18,'fontweight','bold')
    xlabel('k_\theta (cm^-^1)','fontsize',18,'fontweight','bold')
    set(gca,'fontsize',18,'fontweight','bold',...
     'XMinorGrid','on','YMinorGrid','On','Linewidth',2)
    title(strcat('$\psi=$',num2str(psi(k))),'interpreter','latex')
    pause(1)
%    print(gcf,'-dpng',strcat('./qh_t21_n_omp_kfspec',num2str(k,fform),'_omp'));
    clf
end
