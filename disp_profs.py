"""
plot the profiles (at omp) of the parameters that exists in the theoretical
dispersion relation
"""

from boutpy.boutdata import Case

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker, use
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.12/test'


use('Agg')
case = Case(case_path)

psin = case.gf.get_psin(yind='omp')
yind_omp = case.gf.yind_omp
Ni0 = case.collect('N0', yind=yind_omp, nthreads=1).squeeze()
Bxy = case.gf['Bxy'][:, yind_omp].squeeze()
Bbar = case.gf['bmag']
Va = case.di['Va']
Va_prof = Va * Bxy / Bbar / np.sqrt(Ni0)

fig, ax = plt.subplots(2, 3, figsize=[18, 10], facecolor='w')
fig.subplots_adjust(hspace=0.3, wspace=0.65)
ax = ax.flatten()
ax[0].plot(psin, Va_prof, 'b-')
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel(r'$V_A$ (m/s)')
ax[0].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[0].grid(True)


eta = case.collect('eta', yind=yind_omp, zind=15, nthreads=1).squeeze()
Lbar = case.di['Lbar']
MU0 = 4.0e-7 * np.pi
# in Ohm-m
eta *= Va * Lbar * MU0
ax[1].plot(psin, eta, 'b-')
ax[1].set_xlabel(r'$\psi_n$')
ax[1].set_ylabel(r'$\eta\ \mathrm{\left(\Omega\cdot m\right)}$')
ax[1].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[1].grid(True)

# this is -Vdi (ion diamagnetic velocity)
Ve0_diay = case.collect('Ve0_diay', yind=yind_omp, nthreads=1).squeeze()
hthe = case.gf['hthe'][:, yind_omp].squeeze()
Ve0_diay *= (hthe / Lbar)
Bpxy = case.gf['Bpxy'][:, yind_omp].squeeze()
Ve0_dia_theta = Ve0_diay / Bxy * Bpxy
# assume Vde = -Vdi
# Vde = Ve0_diay * Va
Vde = Ve0_dia_theta * Va
ax[2].plot(psin[2: -2], Vde[2: -2], 'b-')
# ax[2].plot(psin, Vde, 'b-')
ax[2].set_xlabel(r'$\psi_n$')
ax[2].set_ylabel(r'$v_{De}$ (m/s)')
ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))
ax[2].grid(True)

AA = float(case.inp['highbeta']['AA'])
gyro_frq = 9.58e3 / AA * Bxy * 1.0e4
Rxy = case.gf['Rxy'][:, yind_omp].squeeze()
ax[3].plot(psin, Rxy, 'b-')
ax[3].set_xlabel(r'$\psi_n$')
ax[3].set_ylabel(r'$R$ (m)')
ax[3].grid(True)
ax[4].plot(psin, Bxy, 'b-')
ax[4].set_xlabel(r'$\psi_n$')
ax[4].set_ylabel(r'$B$ (T)')
ax[4].grid(True)
ax[5].plot(psin, gyro_frq, 'b-')
ax[5].set_xlabel(r'$\psi_n$')
ax[5].set_ylabel(r'$\Omega_{i}\ \mathrm{(s^{-1})}$')
ax[5].grid(True)
ax[5].yaxis.set_major_formatter(ticker.FormatStrFormatter('%.2e'))

outname = os.path.join(case.eq_path, 'disp_profs_neo.png')
fig.savefig(outname)
print('Figures have been written to ' + outname)
