from boutpy.boutdata import Case
from boutpy.boututils import DataFile

import numpy as np
import os
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import rcParams

# This script writes the net and diamagnetic parts of the experimental Er
#  of a grid using one of the cases that uses the grid.

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.11/hd4paru1e-8_hd4parapar1e-6'
grid_path = '/vol6/home/chaodong/gridfiles/cmod1120907032/'

matplotlib.use('Agg')
case = Case(case_path)
Va = case.di['Va']
Bbar = case.di['Bbar']
Lbar = case.di['Lbar']
Rxy = case.gf['Rxy']
Bpxy = case.gf['Bpxy']

Er0_dia_x = case.collect('Er0_dia_x', nthreads=1).squeeze()
Er0_x = case.collect('Er0_x', nthreads=1).squeeze()
Er0_dia = Er0_dia_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)
Er0_dia[:2, :] = 0
Er0_dia[-2:, :] = 0
Er0 = Er0_x * Va * Bbar * (Rxy / Lbar) * (Bpxy / Bbar)

Er0_net = Er0 - Er0_dia

fig, ax = plt.subplots(1, 2, figsize=[14, 6], facecolor='w')
ax = ax.flatten()
fig.subplots_adjust(wspace=0.8)
psin, yind_imp, yind_omp = case.gf.get_psin(yind='omp', index=True)
Zxy = case.gf['Zxy']

ct0 = ax[0].contourf(Rxy, Zxy, Er0_net, cmap=plt.get_cmap('jet'), levels=200)
fig.colorbar(ct0, ax=ax[0], fraction=0.08, aspect=30)
ax[0].set_xlabel(r'R (m)')
ax[0].set_ylabel(r'Z (m)')
ax[0].set_title(r'$E_{r, net}$ (V/m)')

ax[1].plot(psin, Er0[:, yind_omp], 'b-', label=r'$E_{r,exp}$')
ax[1].plot(psin, Er0_dia[:, yind_omp], 'r-', label=r'$E_{r,dia}$')
ax[1].plot(psin, Er0_net[:, yind_omp], 'g-', label=r'$E_{r,net}$')
ax[1].set_label(r'$\psi_n$')
ax[1].set_ylabel(r'$E_r$ (V/m)')
ax[1].legend()
ax[1].grid(True)

outname = os.path.join(grid_path, 'Er_components.png')
fig.savefig(outname)
print('Figures have been written to ' + outname)

nc_name = os.path.join(grid_path, 'Er_components.nc')
nc_file = DataFile(nc_name, write=True, create=True)
nc_file.write('Er0', Er0.view(np.ndarray))
nc_file.write('Er0_dia', Er0_dia.view(np.ndarray))
nc_file.write('Er0_net', Er0_net.view(np.ndarray))
nc_file.close()
print('Data have been written to ' + nc_name)
