from boutpy.boutdata import Case

import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
import matplotlib

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
# case_path = '/vol6/home/chaodong/BoutCases/2021.01/thermal_forceF_eHallF/n_scan/n50'

var1_name = 'Te'
var2_name = 'phi'
# psin = 0.975
psin = 0.999
yind = 76
tl = 1400
tr = 1500
# the harmonics to show
harmonics = [9, 10, 11]
# harmonics = [1]

matplotlib.use('Agg')
case = Case(case_path)
xind, _ = case.gf.get_xind(psin)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
zperiod = int(case.inp['ZPERIOD'])

var1 = case.collect(var1_name, xind=xind, yind=yind, tind=[tl, tr], nthreads=1).squeeze()
var2 = case.collect(var2_name, xind=xind, yind=yind, tind=[tl, tr], nthreads=1).squeeze()

var1_k = np.fft.rfft(var1, axis=0)
var2_k = np.fft.rfft(var2, axis=0)
t = case.t_array[tl: tr + 1]

fig, ax = plt.subplots(figsize=[6, 6], facecolor='white')
ax.grid(True)
for iharmonic in harmonics:
    # var1_phase = np.angle(var1_k[iharmonic, :]).squeeze()
    # var2_phase = np.angle(var2_k[iharmonic, :]).squeeze()
    # cross_phase = var1_phase - var2_phase
    cross_phase = np.angle(var1_k[iharmonic, :] / var2_k[iharmonic, :])
    # for it in range(0, tr - tl + 1):
    #     if cross_phase[it] > np.pi:
    #         cross_phase[it] = cross_phase[it] - 2 * np.pi
    #     elif cross_phase[it] < -np.pi:
    #         cross_phase[it] = cross_phase[it] + 2 * np.pi
    ax.plot(t, cross_phase, label='n={:02d}'.format(iharmonic * zperiod))
lgd = ax.legend()
plt.show()
ax.set_xlabel(r't $\left(\tau_{A}\right)$')
var1_latex, _ = case.di.get_varname_latex(var1_name)
var2_latex, _ = case.di.get_varname_latex(var2_name)
ax.set_ylabel(r'$\theta_{' + var1_latex +
              r'}-\theta_{' + var2_latex + '}$')

outname = os.path.join(
    case.pert_path, 'cross_phase_' + var1_name + '-' + var2_name +
                    '_psin{:.4f}_iy{:03d}_t{:04d}-{:04d}2.png'.format(
                        psin, yind, int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname)
print('Figures have been written to ' + outname)
