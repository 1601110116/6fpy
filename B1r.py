from boutpy.boutdata import poloidal_slice
from boutpy.boutdata import Case
from boutpy.boutdata import collect

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
import matplotlib

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
nlevels = 150
time = 1500

case = Case(case_path=case_path)
matplotlib.use('Agg')
rcParams.update(
    {"font.size": 20,
     "legend.fontsize": 10,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "savefig.bbox": "tight"}
)
matplotlib.use('Agg')
tind = case.get_tstep(time)
B0 = case.collect('B0', nthreads=1)
Vbtildx = case.collect('Vbtildx', tind=tind, nthreads=1).squeeze()
# Vbtildx = Vbtildx / np.reshape(B0, (case.di.nx, case.di.ny, 1))
Vbtildx_rms = Vbtildx.std(axis=-1)
Rxy = case.gf['Rxy']
Lbar = case.di['Lbar']
Bpxy = case.gf['Bpxy']
Bbar = case.di['Bbar']
Vbtildr_rms = Vbtildx_rms / ((Rxy/Lbar) * (Bpxy/Bbar))

Zxy = case.gf['Zxy']
fig, ax = plt.subplots(figsize=(8, 12.4))
ct = ax.contourf(Rxy, Zxy, Vbtildr_rms, levels=200, cmap=plt.get_cmap('jet'))
cbar = fig.colorbar(ct, ax=ax, fraction=0.08, aspect=40)
cbar.ax.set_ylabel(r'$\left(B_{1,r}/B_0\right)_{rms}$')
ax.plot(Rxy[0, :], Zxy[0, :], 'k-')
ax.plot(Rxy[-1, :], Zxy[-1, :], 'k-')
ixseps1 = case.gf['ixseps1']
ax.plot(Rxy[ixseps1, :], Zxy[ixseps1, :], 'k-', lw=1)

psin, yind_imp, yind_omp = case.gf.get_psin(
    yind='omp', index=True, verbose=False)
ax.plot(Rxy[:, yind_omp], Zxy[:, yind_omp], 'g-', lw=2)
ax.set_xlabel(r'R $\left(\mathrm{m}\right)$')
ax.set_ylabel(r'Z $\left(\mathrm{m}\right)$')

outname = os.path.join(case.pert_path, 'B1r_t{:04d}_noB0'.format(int(case.get_time(tind))))
fig.savefig(outname + '.png', dpi=120)
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)

