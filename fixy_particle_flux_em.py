from boutpy.boutdata import Case
from boutpy.boutdata import collect

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
# tl = 1400  # in Alfven time
# tr = 1500
tl = 1080  # in Alfven time
tr = 1080
dt = 5
psinl = 0.945
psinr = 1.0075
yind = 76
include_em = True

case = Case(case_path)
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
timestep = case.di.get_timestep()
dt = floor(float(dt) / timestep)
print('tl_ind={}, tr_ind={}, dt={}'.format(tl, tr, dt))
tind = np.asarray(list(range(tl, tr+1, dt)))
nt = len(tind)
psin = case.gf.get_psin(yind='omp')
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)
print('xindl={}, xindr={}'.format(xindl, xindr))

Va = case.di['Va']
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Nbar = case.di['Nbar']
density = case.di['density']
Rx = case.gf['Rxy'][:, yind].squeeze()
Bpx = case.gf['Bpxy'][:, yind].squeeze()
psix = case.gf['psixy'][:, yind].squeeze()
nx = case.di.nx
ny = case.di.ny
nz = case.di.nz

Ni = np.zeros([nx, nz, nt])
Vexbx = Ni.copy()
print('collecting Ni and Vexbx : ')
for it in range(nt):
    print('\ttind={}'.format(tind[it]))
    Ni[:, :, it] = case.collect('Ni', tind=tind[it], yind=yind, nthreads=1).squeeze()
    Vexbx[:, :, it] = case.collect('Vexbx', tind=tind[it], yind=yind, nthreads=1).squeeze()
print('collecting N0 :')
Ni0 = case.collect('N0', yind=yind, nthreads=1).squeeze()
Ni_tot = Ni + np.reshape(Ni0, (nx, 1, 1))

# the radial component is determined solely by the contravariant x component
Vexbr = Vexbx / np.reshape((Rx/Lbar) * (Bpx/Bbar), (nx, 1, 1))

Nirflux_es = Ni_tot * Vexbr * (Nbar * density/1e20 * Va)
Nirflux_es_zavg = np.mean(Nirflux_es, axis=1)
Nirflux_es_zavg = Nirflux_es_zavg[xindl: xindr+1, :]
if include_em:
    jpar = np.zeros([nx, nz, nt])
    Vbtildx = np.zeros([nx, nz, nt])
    for it in range(nt):
        jpar[:, :, it] = case.collect('jpar', tind=tind[it], yind=yind, nthreads=1).squeeze()
        Vbtildx[:, :, it] = case.collect('Vbtildx', tind=tind[it], yind=yind, nthreads=1).squeeze()
    J0 = case.collect('J0', yind=yind, nthreads=1)
    J_tot = jpar + np.reshape(J0, (nx, 1, 1))
    B0 = case.collect('B0', yind=yind, nthreads=1)
    Vbtildx = Vbtildx / np.reshape(B0, (nx, 1, 1))
    Vbtildr = Vbtildx / np.reshape((Rx/Lbar) * (Bpx/Bbar), (nx, 1, 1))
    ee = 1.602176565e-19
    MU0 = 4e-7 * np.pi
    Ne_paraflux = -1/ee * Bbar / (MU0 * Lbar) * jpar / 1e20
    Nerflux_em = Ne_paraflux * Vbtildr
    Nerflux_em_zavg = np.mean(Nerflux_em, axis=1)
    Nerflux_em_zavg = Nerflux_em_zavg[xindl: xindr+1, :]
    Nerflux_em_tavg = np.mean(Nerflux_em_zavg, axis=1)

fig, ax = plt.subplots(figsize=[7, 5], facecolor='white')

Nirflux_es_tavg = np.mean(Nirflux_es_zavg, axis=1)
ax.plot(psin[xindl: xindr+1], Nirflux_es_tavg, 'b-', label='es')
if include_em:
    ax.plot(psin[xindl: xindr+1], Nerflux_em_tavg, 'k-', label='em')
    ax.legend()
ax.set_xlabel(r'$\psi_n$')
ax.set_ylabel(r'$\left<\Gamma_{r}\right>_\zeta\ \left(\mathrm{10^{20}m^{-3}m/s}\right)$')
ax.grid(True)
xlim = ax.get_xlim()
ylim = ax.get_ylim()
# ax.text(x=xlim[0], y=ylim[1], s='(a)', va='top', ha='left')

outname = os.path.join(case.pert_path, 'particle_flux_t{:04d}-{:04d}_y{:03d}'.format(
    int(case.get_time(tl)), int(case.get_time(tr)), yind))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
