from boutpy.boutdata import Case
from boutpy.boutdata import collect

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 22,
     "legend.fontsize": 22,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)
cases = ['/vol6/home/chaodong/BoutCases/2021.01/nl_Btscan/Bt0.8x',
         '/vol6/home/chaodong/BoutCases/2021.01/nl_Btscan/Bt0.9x',
         '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar',
         '/vol6/home/chaodong/BoutCases/2021.01/nl_Btscan/Bt1.1x',
         '/vol6/home/chaodong/BoutCases/2021.01/nl_Btscan/Bt1.2x']
labels = [r'0.8$\times$',
          r'0.9$\times$',
          r'1.0$\times$',
          r'1.1$\times$',
          r'1.2$\times$']

tl = 1400  # in Alfven time
tr = 1500
dt = 5
psinl = 0.945
psinr = 1.0075

matplotlib.use('Agg')
# All cases should have the same following parameters
case = Case(cases[0])
tl = case.get_tstep(tl)
tr = case.get_tstep(tr)
timestep = float(case.inp['TIMESTEP'])
dt = floor(float(dt) / timestep)
print('tl_ind={}, tr_ind={}, dt={}'.format(tl, tr, dt))
tind = np.asarray(list(range(tl, tr+1, dt)))
nt = len(tind)
psin, yind_imp, yind_omp = case.gf.get_psin(yind='omp', index=True)
xindl, _ = case.gf.get_xind(psinl)
xindr, _ = case.gf.get_xind(psinr)
print('xindl={}, xindr={}'.format(xindl, xindr))

Va = case.di['Va']
Lbar = case.di['Lbar']
Bbar = case.di['Bbar']
Nbar = case.di['Nbar']
density = case.di['density']
Rxy = case.gf['Rxy']
Bpxy = case.gf['Bpxy']
psixy = case.gf['psixy']
nx = case.di.nx
ny = case.di.ny
nz = case.di.nz

fig, ax = plt.subplots(1, 2, figsize=[12, 6], facecolor='white')
fig.subplots_adjust(hspace=0.3, wspace=0.4)
ax[0].grid(True)
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel(r'$n_{i,rms}\ \left(\mathrm{10^{{20}}\ m^{{-3}}}\right)$')
ax[1].grid(True)
ax[1].set_xlabel(r'$\psi_n$')
ax[1].set_ylabel(r'$\left<\Gamma_{r}\right>\ (\mathrm{10^{20}m^{-3}m/s})$')

Ni = np.zeros([nx, ny, nz, nt])
Vexbx = Ni.copy()
for icase in range(len(cases)):
    case = Case(cases[icase])
    print('collecting Ni and Vexbx : ')
    for it in range(nt):
        print('\ttind={}'.format(tind[it]))
        Ni[:, :, :, it] = case.collect('Ni',  tind=tind[it], nthreads=1).squeeze()
        Vexbx[:, :, :, it] = case.collect('Vexbx', tind=tind[it], nthreads=1).squeeze()
    print('collecting N0 :')
    Ni0 = collect('N0', path=case.data_path, nthreads=1)
    Ni_tot = Ni + np.reshape(Ni0, (nx, ny, 1, 1))

    Ni_omp = Ni[:, yind_omp, :, :]
    Nirms = Ni_omp.std(axis=1)
    _, Ninorm = case.di.get_normalization('Ni', value=True)
    Nirms *= Ninorm
    Nirms_tavg = Nirms.mean(axis=-1).squeeze()
    ax[0].plot(psin[xindl: xindr + 1], Nirms_tavg[xindl: xindr + 1], label=labels[icase])

    # the radial component is determined solely by the contravariant x component
    Vexbr = Vexbx / np.reshape((Rxy/Lbar) * (Bpxy/Bbar), (nx, ny, 1, 1))
    # Vexbr = np.ones_like(Ni)

    # We only include electrostatic Ni flux for now, since we assume Vi=0
    # Ni0 is neglected since we do not have equilibrium radial EXB flow, <Ni0*Vexbr>=0
    Nirflux_es = Ni_tot * Vexbr * (Nbar * density/1e20 * Va)
    Nirflux_es_surface_avg = case.gf.surface_avg(Nirflux_es)
    Nirflux_es_surface_avg = Nirflux_es_surface_avg[xindl: xindr+1, :]
    Nirflux_es_tavg = np.mean(Nirflux_es_surface_avg, axis=1)
    ax[1].plot(psin[xindl: xindr+1], Nirflux_es_tavg, label=labels[icase])

xlim = ax[0].get_xlim()
ylim = ax[0].get_ylim()
ax[0].text(x=xlim[0], y=ylim[1], s='(a)', va='top', ha='left')
# ax[0].legend(loc='upper left')
ax[0].legend()
xlim = ax[1].get_xlim()
ylim = ax[1].get_ylim()
ax[1].text(x=xlim[0], y=ylim[1], s='(b)', va='top', ha='left')
ax[1].legend()

outname = os.path.join(case.pert_path,
                       'Nirms_flux_t{:04d}-{:04d}'.format(int(case.get_time(tl)), int(case.get_time(tr))))
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
