import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import os

from boutpy.boutdata import Case
from boutpy.boutdata import collect
from boutpy.boututils.interp_smooth import smooth

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 4,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2020.07/expEr/U1e-2hd4par1e-5_nscan/n35'
tl = 500  # in Alfven time
tr = -451  #

case = Case(case_path)
tl = case.di.get_tstep(tl)
tr = case.di.get_tstep(tr)
print('tl={}, tr={}'.format(tl, tr))

psin = case.gf.get_psin(yind='omp')
fig, ax = plt.subplots(2, 2, figsize=(16, 10))
ax = ax.flatten()
fig.subplots_adjust(hspace=0.2, wspace=0.2, right=0.92)

Ni_tr = collect('Ni', path=case.data_path, tind=tr, nthreads=1).squeeze()
Ni_rms = Ni_tr.std(axis=-1)
ax[0].contourf(psin, range(case.di.ny), Ni_rms.transpose(),
               cmap=plt.get_cmap('jet'), levels=200)
ax[0].set_xlabel(r'$\psi_n$')
ax[0].set_ylabel('yind')
ax[0].set_title('Ni')
x, y = np.unravel_index(np.argmax(Ni_rms), Ni_rms.shape)
#x = 93
#y = 27
ax[0].plot(psin[x], y, 'wx', ms=18, lw=2)
ax[0].legend(title='x={},y={}'.format(x, y))

phi_tr = collect('phi', path=case.data_path, tind=tr, nthreads=1).squeeze()
phi_rms = phi_tr.std(axis=-1)
ax[2].contourf(psin, range(case.di.ny), phi_rms.transpose(),
               cmap=plt.get_cmap('jet'), levels=200)
ax[2].set_xlabel(r'$\psi_n$')
ax[2].set_ylabel('yind')
ax[2].set_title('phi')
ax[2].plot(psin[x], y, 'wx', ms=18, lw=2)

Ni_zt = collect('Ni', path=case.data_path, xind=x, yind=y, tind=[tl, tr], nthreads=1).squeeze()
phi_zt = collect('phi', path=case.data_path, xind=x, yind=y, tind=[tl, tr], nthreads=1).squeeze()
Ni_zt_rms = Ni_zt.std(axis=0)
t_array = case.di['t_array']
t = t_array[tl: tr + 1]
ax[1].plot(t, np.log(Ni_zt_rms))
ax[1].set_xlabel(r't $(\tau_A)$')
ax[1].set_ylabel(r"$\ln(Ni_{rms})$")
cross_phase = np.zeros(Ni_zt.shape[-1])

for it in range(tr - tl + 1):
    Ni_z = Ni_zt[:, it]
    Ni_k = np.fft.rfft(Ni_z)
    Ni_ph = np.angle(Ni_k[1])
    phi_z = phi_zt[:, it]
    phi_k = np.fft.rfft(phi_z)
    phi_ph = np.angle(phi_k[1])
    dif = Ni_ph - phi_ph
    if dif > np.pi:
        dif -= 2 * np.pi
    elif dif < -np.pi:
        dif += 2 * np.pi
    cross_phase[it] = dif

ax[3].plot(t, cross_phase)
# cross_phase = smooth(cross_phase, 5)
rms_peaks = []
for it in range(1, tr - tl):
    if np.max([Ni_zt_rms[it+1], Ni_zt_rms[it-1]]) < Ni_zt_rms[it]:
        rms_peaks.append(it)
print(rms_peaks)
if len(rms_peaks) > 2:
    for i_peak in range(1, 4):
        ax[1].axvline(x=t[rms_peaks[-i_peak]], color='r')
    ax[3].plot(t[rms_peaks[-3:]], cross_phase[rms_peaks[-3:]], 'r+')
    ph_mean = np.mean(cross_phase[rms_peaks[-3:]])
    ax[3].axhline(y=ph_mean, color='b', label='avg={}'.format(ph_mean))
    # ax[1].axvspan(t[rms_peaks[-3]], t[rms_peaks[-1]], alpha=0.5)
    # ax[3].plot(t[rms_peaks[-3:]], np.tile(ph_mean, 3), ls='--',
    #            label='avg={}'.format(ph_mean))

ax[3].set_xlabel(r't $(\tau_A)$')
ax[3].set_ylabel(r'$S_{n}-S_{\phi}$')
ax[3].legend()
plt.savefig(os.path.join(case.pert_path, 't{:04d}-{:04d}adiabaticity2.png'.format(tl, tr)))
