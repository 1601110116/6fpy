from boutpy.boutdata import Case

import numpy as np
from scipy import integrate
from math import floor
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 19,
     "legend.fontsize": 19,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)

case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
tls = [755, 1005, 1255, 1630]  # in Alfven time
trs = [1000, 1250, 1625, 2000]
# tls = [1500, 1510, 1515]
# trs = [1505, 1510, 1515]
em = True

matplotlib.use('Agg')
case = Case(case_path)
tls = np.asarray(tls)
trs = np.asarray(trs)

weights = (trs - tls) / case.di.timestep + 1
weights = weights / np.sum(weights)
print('weights:')
print(weights)
n_intervals = len(tls)

# read common information
data_file1 = os.path.join(case.pert_path,
            'radial_heat_flux_t{:04d}-{:04d}.npz'.format(tls[0], trs[0]))
with np.load(data_file1) as data:
    xindl = data['xindl']
    xindr = data['xindr']
    ngradTe_surface_avg = data['ngradTe_surface_avg']
    ngradTi_surface_avg = data['ngradTi_surface_avg']
heatflux_e_tot = np.zeros(xindr - xindl + 1)
heatflux_i_tot = np.zeros(xindr - xindl + 1)
if em:
    heatflux_e_em = np.zeros(xindr - xindl + 1)
    heatflux_i_em = np.zeros(xindr - xindl + 1)

for i_interval in range(n_intervals):
    print('interval {} of {}'.format(i_interval, n_intervals))
    tl = tls[i_interval]
    tr = trs[i_interval]
    data_file_name = os.path.join(case.pert_path, 'radial_heat_flux_t{:04d}-{:04d}.npz'.format(tl, tr))
    print('loading ' + data_file_name + ' ...')
    with np.load(data_file_name) as data:
        assert xindl == data['xindl']
        assert xindr == data['xindr']
        heatflux_e_es_tavg = data['heatflux_e_es_tavg']
        heatflux_i_es_tavg = data['heatflux_i_es_tavg']
        heatflux_e_em_tavg = data['heatflux_e_em_tavg']
        heatflux_i_em_tavg = data['heatflux_i_em_tavg']
    heatflux_e_tot += weights[i_interval] * (heatflux_e_es_tavg + heatflux_e_em_tavg)
    heatflux_i_tot += weights[i_interval] * (heatflux_i_es_tavg + heatflux_i_em_tavg)
    if em:
        heatflux_e_em += weights[i_interval] * heatflux_e_em_tavg
        heatflux_i_em += weights[i_interval] * heatflux_i_em_tavg
psin = case.gf.get_psin(yind='omp')

print('plotting ...')
fig1, ax1 = plt.subplots(figsize=[6, 6], facecolor='white')
ee = 1.6021766208e-19
ax1.plot(psin[xindl: xindr + 1], 1e20 * ee * 1e-6 * heatflux_e_tot, 'b-',
         label=r'$\left<q_{e}\right>\ \left(\mathrm{MW/m^2}\right)$')
ax1.plot(psin[xindl: xindr + 1], 1e20 * ee * 1e-6 * heatflux_i_tot, 'r-',
         label=r'$\left<q_{i}\right>\ \left(\mathrm{MW/m^2}\right)$')
if em:
    ax1.plot(psin[xindl: xindr + 1], 1e20 * ee * 1e-6 * heatflux_e_em, color='darkblue',
             label=r'$\left<q_{e,em}\right>\ \left(\mathrm{MW/m^2}\right)$')
    ax1.plot(psin[xindl: xindr + 1], 1e20 * ee * 1e-6 * heatflux_i_em, color='darkred',
             label=r'$\left<q_{i,em}\right>\ \left(\mathrm{MW/m^2}\right)$')
ax1.set_ylabel('heat flux')
ax1.set_xlabel(r'$\psi_n$')
ax1.grid(True)
ax1.legend()
outname1 = os.path.join(case.pert_path, 'radial_heat_flux_t{:04d}-{:04d}'.format(tls[0], trs[-1]))
if em:
    outname1 += 'em'
fig1.savefig(outname1 + '.png')
fig1.savefig(outname1 + '.eps')
print('Figures have been written to ' + outname1)
