import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib
import os

rcParams.update(
    {"font.size": 20,
     "legend.fontsize": 20,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 5,
     "lines.markeredgewidth": 2,
     "savefig.bbox": "tight"}
)
# multip = [0.8, 0.9, 1.0, 1.1, 1.2]
# gamma = [0.0159, 0.0172, 0.0183, 0.0192, 0.0200]
Cd = [0.6, 0.8, 1.0, 1.2, 1.4]
frq = [0.0111, 0.0143, 0.0167, 0.0177, 0.0182]
gamma = [0.0143, 0.0172, 0.0183, 0.0184, 0.0178]
frq_ylim = [0, 0.021]
gamma_ylim = [0, 0.0200]

# output_path = '/vol6/home/chaodong/BoutCases/2021.01/n50_resist_scan'
output_path = '/vol6/home/chaodong/BoutCases/2021.09/eHall_tf_coeff_scan_n50'

matplotlib.use('Agg')

fig, ax = plt.subplots(1, 2, figsize=[11, 4], facecolor='white')
fig.subplots_adjust(wspace=0.5)
ax = ax.flatten()
ax[0].plot(Cd, frq, 'bo')
ax[0].grid(True)
# ax[0].set_xlabel(r'multiply resistivity by')
ax[0].set_xlabel(r'$C_d$')
ax[0].set_ylabel(r'$f\ \left(\mathrm{\tau_{A}^{-1}}\right)$')
ax[0].set_ylim(frq_ylim)

ax[1].plot(Cd, gamma, 'bo')
ax[1].grid(True)
# ax[1].set_xlabel(r'multiply resistivity by')
ax[1].set_xlabel(r'$C_d$')
ax[1].set_ylabel(r'$\gamma\ \left(\mathrm{\tau_{A}^{-1}}\right)$')
ax[1].set_ylim(gamma_ylim)

outname = os.path.join(output_path, 'linear_growth_eHall_tf_coeff_scan')
fig.savefig(outname + '.png')
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)
