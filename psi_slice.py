from boutpy.boutdata import poloidal_slice
from boutpy.boutdata import Case
from boutpy.boutdata import collect

import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams, ticker
import matplotlib

# case_path = '/vol6/home/chaodong/BoutCases/2020.12/Rinnustar'
case_path = '/vol6/home/chaodong/BoutCases/2021.07/in1e-15out1e-14_nl'
nlevels = 150
time = 1080

case = Case(case_path=case_path)
matplotlib.use('Agg')
rcParams.update(
    {"font.size": 20,
     "legend.fontsize": 10,
     "legend.labelspacing": 0.1,
     "legend.frameon": False,
     "lines.linewidth": 2,
     "lines.markersize": 18,
     "savefig.bbox": "tight"}
)
tind = case.get_tstep(time)
var = case.collect('Psi', tind=tind, nthreads=1).squeeze()
_, norm = case.di.get_normalization("Psi", value=True)
var *= norm * 1e6  # so the unit is micrometer
rxy_interp, zxy_interp, var_interp = poloidal_slice(var, case_path=case_path, zangle=0.0)

vmax = var_interp.max()
vmin = var_interp.min()

if not vmax > vmin:
    vmax += 1

max_abs = np.max([np.abs(vmin), np.abs(vmax)])
vmin = -max_abs
vmax = max_abs

levels = np.linspace(vmin, vmax, nlevels, endpoint=True)
fig, ax = plt.subplots(figsize=(8, 12.4))
ct = ax.contourf(rxy_interp, zxy_interp, var_interp, levels=levels, cmap=plt.get_cmap('bwr'))
cbar = fig.colorbar(ct, ax=ax, fraction=0.08, aspect=40)
cbar.ax.set_ylabel(r'$\tilde{\psi}\ \left(\mathrm{\mu m}\right)$')
ax.plot(rxy_interp[0, :], zxy_interp[0, :], 'k-')
ax.plot(rxy_interp[-1, :], zxy_interp[-1, :], 'k-')
ixseps1 = case.gf['ixseps1']
ax.plot(rxy_interp[ixseps1, :], zxy_interp[ixseps1, :], 'k-', lw=1)
Rxy = case.gf['Rxy']
Zxy = case.gf['Zxy']
psin, yind_imp, yind_omp = case.gf.get_psin(
    yind='omp', index=True, verbose=False)
ax.plot(Rxy[:, yind_omp], Zxy[:, yind_omp], 'g-', lw=2)
ax.set_xlabel(r'R $\left(\mathrm{m}\right)$')
ax.set_ylabel(r'Z $\left(\mathrm{m}\right)$')
ax.axis('equal')

outname = os.path.join(case.pert_path, 'slice_Psi_t{:04d}'.format(int(case.get_time(tind))))
fig.savefig(outname + '.png', dpi=120)
fig.savefig(outname + '.eps')
print('Figures have been written to ' + outname)

